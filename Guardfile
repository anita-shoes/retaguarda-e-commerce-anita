require 'active_support/inflector'

ignore([%r{^bin/*}, %r{^coverage/*}, %r{^config/*}, %r{^node_modules/*}, %r{^vendor/*}, %r{^db/*}, %r{^lib/*}, %r{^log/*}, %r{^public/*}, %r{^tmp/*}])

# A sample Guardfile
# More info at https://github.com/guard/guard#readme

## Uncomment and set this to only include directories you want to watch
# directories %w(app lib config test spec features) \
#  .select{|d| Dir.exists?(d) ? d : UI.warning("Directory #{d} does not exist")}

## Note: if you are using the `directories` clause above and you are not
## watching the project directory ('.'), then you will want to move
## the Guardfile to a watched dir and symlink it back, e.g.
#
#  $ mkdir config
#  $ mv Guardfile config/
#  $ ln -s config/Guardfile .
#
# and, you'll have to watch "config/Guardfile" instead of "Guardfile"

guard :minitest, spring: "bin/rails test", all_on_start: false do
  watch(%r{^test/(.*)/?(.*)_test\.rb$})
  watch('test/test_helper.rb') { 'test' }
  watch('app/policies/application_policy.rb') { 'test' }
  watch('app/controllers/application_controller.rb') { 'test' }
  watch('config/routes.rb')    { integration_tests }
  watch(%r{^app/models/(.*?)\.rb$}) do |matches|
    "test/models/#{matches[1]}_test.rb"
  end
  watch(%r{^app/services/(.*?)\.rb$}) do |matches|
    "test/services/#{matches[1]}_test.rb"
  end
  watch(%r{^app/helpers/btms/(.*?)\.rb$}) do |matches|
    "test/helpers/btms/#{matches[1]}_test.rb"
  end
  watch(%r{^app/models/refatorar/(.*?)\.rb$}) do |matches|
    "test/models/#{matches[1]}_test.rb"
  end
  watch(%r{^test/fixtures/(.*?)\.yml$}) do |matches|
    "test/models/#{matches[1].singularize}_test.rb"
  end
  watch(%r{^app/controllers/(.*?)_controller\.rb$}) do |matches|
    resource_tests(matches[1])
  end
  watch(%r{^app/policies/(.*?)_policy\.rb$}) do |matches|
    "test/policies/#{matches[1]}_policy_test.rb"
  end
  watch(%r{^app/views/([^/]*?)/.*\.html\.erb$}) do |matches|
    ["test/controllers/#{matches[1]}_controller_test.rb"] +
    integration_tests(matches[1])
  end
  watch(%r{^app/helpers/(.*?)_helper\.rb$}) do |matches|
    integration_tests(matches[1])
    helper_tests(matches[1])
  end
  watch(%r{^app/views/layouts/([^/]*?)/.*\.html\.erb$}) do |matches|
    'test/integration/site_layout_test.rb'
  end
  watch('app/helpers/sessions_helper.rb') do
    integration_tests << 'test/helpers/sessions_helper_test.rb'
  end
  watch('app/controllers/sessions_controller.rb') do
    ['test/controllers/sessions_controller_test.rb',
     'test/integration/login_test.rb']
  end
   watch('app/views/layouts/_sidebar.html.erb') do
     'test/controllers/painel_controller_test.rb'
   end
  # watch(%r{app/views/users/*}) do
  #   resource_tests('users') +
  #   ['test/integration/microposts_interface_test.rb']
  # end
end

# Returns the integration tests corresponding to the given resource.
def integration_tests(resource = :all)
  if resource == :all
    Dir["test/integration/*"]
  else
    Dir["test/integration/#{resource}_*.rb"]
  end
end

# Returns the controller tests corresponding to the given resource.
def controller_test(resource)
  "test/controllers/#{resource}_controller_test.rb"
end

# Returns the helper tests corresponding to the given resource.
def helper_tests(resource)
  "test/helpers/#{resource}_helper_test.rb"
end

# Returns all tests for the given resource.
def resource_tests(resource)
  integration_tests(resource) << controller_test(resource)
end
#   require 'ostruct'

#   rspec = OpenStruct.new
#   rspec.spec_dir = 'spec'
#   rspec.spec = ->(m) { "#{rspec.spec_dir}/#{m}_spec.rb" }
#   rspec.spec_helper = "#{rspec.spec_dir}/spec_helper.rb"

#   # matchers
#   rspec.spec_files = /^#{rspec.spec_dir}\/.+_spec\.rb$/

#   # Ruby apps
#   ruby = OpenStruct.new
#   ruby.lib_files = /^(lib\/.+)\.rb$/

#   watch(rspec.spec_files)
#   watch(rspec.spec_helper) { rspec.spec_dir }
#   watch(ruby.lib_files) { |m| rspec.spec.call(m[1]) }

#   # Rails example
#   rails = OpenStruct.new
#   rails.app_files = /^app\/(.+)\.rb$/
#   rails.views_n_layouts = /^app\/(.+(?:\.erb|\.haml|\.slim))$/
#   rails.controllers = %r{^app/controllers/(.+)_controller\.rb$}

#   watch(rails.app_files) { |m| rspec.spec.call(m[1]) }
#   watch(rails.views_n_layouts) { |m| rspec.spec.call(m[1]) }
#   watch(rails.controllers) do |m|
#     [
#       rspec.spec.call("routing/#{m[1]}_routing"),
#       rspec.spec.call("controllers/#{m[1]}_controller"),
#       rspec.spec.call("acceptance/#{m[1]}")
#     ]
#   end

  # TestUnit
  # watch(%r|^test/(.*)_test\.rb$|)
  # watch(%r|^lib/(.*)([^/]+)\.rb$|)     { |m| "test/#{m[1]}test_#{m[2]}.rb" }
  # watch(%r|^test/test_helper\.rb$|)    { "test" }
  # watch(%r|^app/controllers/(.*)\.rb$|) { |m| "test/functional/#{m[1]}_test.rb" }
  # watch(%r|^app/models/(.*)\.rb$|)      { |m| "test/unit/#{m[1]}_test.rb" }
# end
