Rails.application.routes.draw do
  get 'activities/index'
  get 'password_resets/new'
  get 'password_resets/edit'

  if Rails.env.production?
    get '/404', to: "errors#not_found"
    get '/422', to: "errors#unacceptable"
    get '/500', to: "errors#internal_error"
  end

  class OnlyAjaxRequest
     def matches?(request)
       request.xhr?
     end
  end

  root 'application#home'

  # Fbits Migrations
  get '/fbits-migrations', to: 'fbits_migrations#index'
  get '/fbits-migrations/products-nerus-2-tray', to: 'fbits_migrations#products_nerus_2_tray', as: 'products_nerus_2_tray'
  get '/fbits-migrations/clients', to: 'fbits_migrations#migrate_clients', as: 'migrate_clients'
  get '/fbits-migrations/brands', to: 'fbits_migrations#migrate_brands', as: 'migrate_brands'
  get '/fbits-migrations/categories', to: 'fbits_migrations#migrate_categories', as: 'migrate_categories'
  get '/fbits-migrations/products', to: 'fbits_migrations#migrate_products', as: 'migrate_products'
  get '/fbits-migrations/orders', to: 'fbits_migrations#migrate_orders', as: 'migrate_orders'
  get '/fbits-migrations/orders-yml', to: 'fbits_migrations#migrate_orders_yml', as: 'migrate_orders_yml'

  get '/products' => 'products#index'
  get '/products/:sku' => 'products#index', as: 'product'

  get '/signup', to: 'users#new', as: 'signup'
  post '/signup', to: 'users#create'

  get '/login', to:'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  
  get '/my-profile', to: 'users#my_profile', as: 'my_profile'

  resources :users
  resources :activities
  resources :clients
  resources :orders
  resources :products
  resources :logs, only: [:index]
  resources :account_activations, only: [:edit]
  resources :password_resets, only: [:new, :create, :edit ]
  resources :password_resets, only: [:update], defaults: { format: 'js' }
end
