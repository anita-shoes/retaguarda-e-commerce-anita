# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )

#CSS
Rails.application.config.assets.precompile += %w( icheck/demo/css/custom )

# JS
Rails.application.config.assets.precompile += %w( jquery/dist/jquery.min )
Rails.application.config.assets.precompile += %w( pace.min.js )
Rails.application.config.assets.precompile += %w( metismenu/dist/metisMenu.min )
Rails.application.config.assets.precompile += %w( icheck/icheck )
Rails.application.config.assets.precompile += %w( pace )

# Images
# Rails.application.config.assets.precompile += %w( logo-square-alfa.png )
# Rails.application.config.assets.precompile += %w( favicons/favicon-96x96.png )

