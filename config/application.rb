require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Retaguarda
  class Application < Rails::Application
    config.client = 'anita'

    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2
    
    config.time_zone = 'America/Campo_Grande'
    config.active_record.default_timezone = :local # Or :utc

    I18n.available_locales = [:en, :"pt-BR"]
    I18n.default_locale = :"pt-BR"

    config.autoload_paths += Dir[Rails.root.join("app", "models", "{*/}")]

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    #
    
    config.assets.version = '1.0'
    config.fbits_api_uri = 'https://api.fbits.net/'
    config.token = 'Basic anita-6b7317f3-6c5b-4222-b66a-210bffaa5629'

    config.pictures_swap = "tmp/pictures_swap/"
    config.pictures_review = "tmp/pictures_review/"
    config.mail_password = 'amesmadeantes'
  end
end
