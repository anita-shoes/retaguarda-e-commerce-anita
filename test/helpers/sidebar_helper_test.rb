require 'test_helper'

class SidebarHelperTest < ActionView::TestCase
  attr_accessor :controller, :action 

  def params
    { :controller => self.controller, :action => self.action }
  end

  def request
    mock(:path => "my_url")
  end

  test "configurations group opened at user list" do
    self.controller = 'users'
    self.action = 'index'

    assert_equal show_group?('configurations'), true
  end

  test "configurations group closed at user list" do
    self.controller = ''
    self.action = ''

    assert_not show_group?('configurations')
  end

  test "users link is active if params say so" do
    self.controller = 'users'
    self.action = 'index'

    assert_equal active_link?(self.controller, self.action), true
  end

  test "users link is inactive if action is not the right thing" do
    self.controller = 'users'
    self.action = 'index'

    assert_not active_link?(self.controller, 'show')
  end

  test "users link is inactive if controller is not the right thing" do
    self.controller = 'users'
    self.action = 'index'

    assert_not active_link?('activities', self.action)
  end
end
