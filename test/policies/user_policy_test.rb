require 'test_helper'

class UserPolicyTest < ActionDispatch::IntegrationTest
	
	test "Non superuser can edit himself" do
    logged = users(:lana)
    record = users(:lana)

    assert UserPolicy.new(logged, record).update?
  end

  test "Non super user cant edit other user" do
    logged = users(:archer)
    record = users(:lana)

    assert_not UserPolicy.new(logged, record).update?
  end

  test "Super user can edit other user" do
    logged = users(:michael)
    record = users(:lana)

    assert UserPolicy.new(logged, record).update?
  end

  test "Non superuser can't delete himself" do
    logged = users(:lana)
    record = users(:lana)

    assert_not UserPolicy.new(logged, record).destroy?
  end

  test "Non superuser can't delete other user" do
    logged = users(:lana)
    record = users(:archer)

    assert_not UserPolicy.new(logged, record).destroy?
  end

  test "superuser can't delete himself" do
    logged = users(:michael)
    record = users(:michael)

    assert_not UserPolicy.new(logged, record).destroy?
  end

  test "superuser can delete other user" do
    logged = users(:michael)
    record = users(:lana)

    assert UserPolicy.new(logged, record).destroy?
  end

end
