require 'test_helper'

class FbitsApiServiceTest < ActionView::TestCase

  def setup
    @fbits_service = FbitsApiService.new
  end

  test "default vars on initialize" do
    assert_equal @fbits_service.uri, Rails.application.config.fbits_api_uri

    assert @fbits_service.headers.is_a?(Hash)
    assert @fbits_service.headers.include? :Authorization
    assert_equal @fbits_service.headers[:Authorization], Rails.application.config.token

    assert @fbits_service.params.is_a?(Hash)

    assert @fbits_service.campos_adicionais.is_a?(Array)
  end

  test "vars on get products" do
    @fbits_service.expects(:go).returns(true)
    @fbits_service.get_products('2', '15', { categorias: '1,2,3', fabricantes: '4,5,6' })
    assert_equal @fbits_service.data_method, 'GET'
    assert_equal @fbits_service.uri_path, 'produtos'

    assert @fbits_service.params.include? :pagina
    assert_equal @fbits_service.params[:pagina], '2'

    assert @fbits_service.params.include? :quantidadeRegistros
    assert_equal @fbits_service.params[:quantidadeRegistros], '15'

    assert @fbits_service.params.include? :categorias
    assert_equal @fbits_service.params[:categorias], '1,2,3'

    assert @fbits_service.params.include? :fabricantes
    assert_equal @fbits_service.params[:fabricantes], '4,5,6'

  end

  test "mountUrl returns ok" do
    @fbits_service.uri_path = 'path'
    @fbits_service.params = {
      pagina: '3',
      quantidadeRegistros: '7',
      categorias: '9,8,7',
      fabricantes: '6.5.4'
    }

    url = @fbits_service.mount_url
    assert_equal Rails.application.config.fbits_api_uri + @fbits_service.uri_path + '?env=prod' +
    '&pagina=' + @fbits_service.params[:pagina] +
    '&categorias=' + @fbits_service.params[:categorias] +
    '&fabricantes=' + @fbits_service.params[:fabricantes] +
    '&quantidadeRegistros=' + @fbits_service.params[:quantidadeRegistros],
    url
  end

  test "url de get produtos sem parametros" do
    @fbits_service.expects(:go).returns(true)
    @fbits_service.get_products()
    url = @fbits_service.mount_url

    assert_equal Rails.application.config.fbits_api_uri + @fbits_service.uri_path + '?env=prod' +
    '&pagina=1&quantidadeRegistros=10',
    url
  end
end
