require 'test_helper'

class HttpServiceTest < ActionView::TestCase

  def setup
    @http_service = HttpService.new
  end

  test "set url" do
    @http_service.set_uri('http://foo.bar')
    assert_instance_of URI::HTTP, @http_service.uri
  end

  test "post" do
    request = Object.new
    request.class.module_eval { attr_accessor :content_type}
    request.class.module_eval { attr_accessor :body}

    Net::HTTP::Post.expects(:new).at_least_once.returns(true)
    @http_service.expects(:prepare_json_request).returns(true)
    @http_service.expects(:set_response).returns(true)
    @http_service.expects(:do_request).returns(request)

    @http_service.set_uri('http://localhost/path')
    @http_service.json_post({})
    assert @http_service.request
  end

  test 'set request test' do
    @http_service.set_uri('http://localhost/path')

    Net::HTTP::Get.expects(:new).at_least_once
    @http_service.set_requestGet
  end

  test 'set response' do
    @http_service.set_uri('http://localhost/path')
    response = Object.new
    response.class.module_eval { attr_accessor :body}
    Net::HTTP.expects(:start).at_least_once.returns(response)

    @http_service.set_response
    assert @http_service.response
  end

  test "return utf8" do
    response = Object.new
    response.expects(:force_encoding).with('UTF-8').returns('{}')

    result = @http_service.json_utf8(response)
    assert result
  end

end
