require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  
	def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end

  test "should get new" do
    get signup_path
    assert_response :success
  end

  test "non superuser update another user is forbidden" do
    log_in_as(@other_user)
    patch user_path(@user)

    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test "should not allow the admin attribute to be edited via the web" do
    log_in_as(@other_user)
    assert_not @other_user.admin?

    # policy = stub(update?: true, permitted_attributes: [:name])
    # UserPolicy.stubs(:new).with(@other_user, @other_user).returns(policy)

    patch user_path(@other_user), params: {
                                    user: { password: '123456',
                                            password_confirmation: '123456',
                                            admin: true } }
    assert_not @other_user.admin?
    assert_not flash.empty?
    assert_redirected_to user_path(@other_user)
  end

  test "should redirect update when not logged in" do
    log_in_as(@other_user)
    patch user_path(@user), params: { user: { name: @user.name,
                                              email: @user.email } }
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test "should update when own user" do
    log_in_as(@other_user)
    patch user_path(@other_user), params: { user: { name: @user.name,
                                              email: 'foo@bar.com' } }
    
    assert_not flash.empty?
    assert_equal User.find(@other_user.id).email, 'foo@bar.com'
    assert_redirected_to user_path(@other_user)
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete user_path(@user)
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when logged in as a non-superuser" do
    log_in_as(@other_user)
    assert_no_difference 'User.count' do
      delete user_path(@user)
    end
    assert_redirected_to root_url
  end

  test "should destroy when logged in as a superuser" do
    log_in_as(@user)
    assert_difference 'User.count', -1 do
      delete user_path(@other_user)
    end
    assert_redirected_to users_path
  end

end
