require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get login_path
    assert_response :success
  end

  test "should get home" do
    get login_url
    assert_response :success
    assert_select "title", "Login | Retaguarda Anita Shoes"
  end
end
