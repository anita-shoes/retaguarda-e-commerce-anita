require 'test_helper'

class ActivitiesControllerTest < ActionDispatch::IntegrationTest
	def setup
		log_in_as(users(:michael))
	end

  test "should get index" do
    get activities_index_url
    assert_response :success
  end

end
