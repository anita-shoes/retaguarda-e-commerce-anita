
class Relateds_fixture
  attr_accessor :data

  def initialize
    self.data = [
      {
        "produtoId" => '000003',
        "parentId" => '67217',
        "produtoVarianteId" => '253064',
        "sku" => "1004390197"
      },
      {
        "produtoId" => '000004',
        "parentId" => '67217',
        "produtoVarianteId" => '253064',
        "sku" => "1004390197"
      }
    ]
  end

end
