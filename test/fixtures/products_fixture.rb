
class Products_fixture
  attr_accessor :data

  def initialize
    self.data = [
      {
        "produtoVarianteId" => '000001',
        "produtoId" => '10000',
        "idPaiExterno" => "000001-GOLD",
        "sku" => "0000010101",
        "nome" => "Tenis Converse All Star Deluxe Charm Leather  1",
        "precoDe" => '109.99',
        "precoPor" => '109.99',
        "valido" => true,
        "exibirSite" => true,
        "estoque" => [
            {
                "estoqueFisico" => 3,
                "estoqueReservado" => 1
            }
        ],
        "atributos" => [
            {
                "tipoAtributo" => "Selecao",
                "isFiltro" => true,
                "nome" => "COR",
                "valor" => "GOLD",
                "exibir" => true
            },
            {
                "tipoAtributo" => "Selecao",
                "isFiltro" => true,
                "nome" => "COR",
                "valor" => "NUDE",
                "exibir" => true
            },
            {
                "tipoAtributo" => "Selecao",
                "isFiltro" => true,
                "nome" => "TAMANHO",
                "valor" => "34",
                "exibir" => true
            }
        ],
        "dataCriacao" => "2013-06-08T13:36:16.767",
        "dataAtualizacao" => "2018-09-13T04:59:28.43"
      },
      {
        "produtoVarianteId" => '000002',
        "produtoId" => '200000',
        "idPaiExterno" => "000002-AZUL",
        "sku" => "0000020202",
        "nome" => "Tenis Converse All Star Deluxe Charm Leather  2",
        "precoDe" => '159.99',
        "precoPor" => '159.99',
        "valido" => true,
        "exibirSite" => true,
        "estoque" => [
            {
                "estoqueFisico" => 3,
                "estoqueReservado" => 1
            }
        ],
        "atributos" => [
            {
                "tipoAtributo" => "Selecao",
                "isFiltro" => true,
                "nome" => "COR",
                "valor" => "AZUL",
                "exibir" => true
            },
            {
                "tipoAtributo" => "Selecao",
                "isFiltro" => true,
                "nome" => "TAMANHO",
                "valor" => "40",
                "exibir" => true
            }
        ],
        "dataCriacao" => "2013-06-08T13:36:44.973",
        "dataAtualizacao" => "2018-09-13T04:59:29.477"
      }
    ]
  end

  def diff_colors
    {
        "produtoVarianteId" => '000002',
        "produtoId" => '200000',
        "idPaiExterno" => "000002-AZUL",
        "sku" => "0000020202",
        "nome" => "Tenis Converse All Star Deluxe Charm Leather  2",
        "precoDe" => '159.99',
        "precoPor" => '159.99',
        "valido" => true,
        "exibirSite" => true,
        "estoque" => [
            {
                "estoqueFisico" => 3,
                "estoqueReservado" => 1
            }
        ],
        "atributos" => [
            {
                "tipoAtributo" => "Selecao",
                "isFiltro" => true,
                "nome" => "COR",
                "valor" => "FUSC",
                "exibir" => true
            },
            {
                "tipoAtributo" => "Selecao",
                "isFiltro" => true,
                "nome" => "TAMANHO",
                "valor" => "40",
                "exibir" => true
            }
        ],
        "dataCriacao" => "2013-06-08T13:36:44.973",
        "dataAtualizacao" => "2018-09-13T04:59:29.477"
    }
  end

  def same_colors_diff_stock
    [
        {
            "produtoVarianteId" => '000002',
            "produtoId" => '200000',
            "idPaiExterno" => "000002-AZUL",
            "sku" => "0000020202",
            "nome" => "Tenis Converse All Star Deluxe Charm Leather  2",
            "precoDe" => '159.99',
            "precoPor" => '159.99',
            "valido" => true,
            "exibirSite" => true,
            "estoque" => [
                {
                    "estoqueFisico" => 3,
                    "estoqueReservado" => 1
                }
            ],
            "atributos" => [
                {
                    "tipoAtributo" => "Selecao",
                    "isFiltro" => true,
                    "nome" => "COR",
                    "valor" => "AZUL",
                    "exibir" => true
                },
                {
                    "tipoAtributo" => "Selecao",
                    "isFiltro" => true,
                    "nome" => "TAMANHO",
                    "valor" => "40",
                    "exibir" => true
                }
            ],
            "dataCriacao" => "2013-06-08T13:36:44.973",
            "dataAtualizacao" => "2018-09-13T04:59:29.477"
        },
        {
            "produtoVarianteId" => '000003',
            "produtoId" => '20000o',
            "idPaiExterno" => "000002-AZUL",
            "sku" => "0000020203",
            "nome" => "Tenis Converse All Star Deluxe Charm Leather  2",
            "precoDe" => '159.99',
            "precoPor" => '159.99',
            "valido" => true,
            "exibirSite" => true,
            "estoque" => [
                {
                    "estoqueFisico" => 1,
                    "estoqueReservado" => 0
                }
            ],
            "atributos" => [
                {
                    "tipoAtributo" => "Selecao",
                    "isFiltro" => true,
                    "nome" => "COR",
                    "valor" => "AZUL",
                    "exibir" => true
                },
                {
                    "tipoAtributo" => "Selecao",
                    "isFiltro" => true,
                    "nome" => "TAMANHO",
                    "valor" => "39",
                    "exibir" => true
                }
            ],
            "dataCriacao" => "2013-06-08T13:36:44.973",
            "dataAtualizacao" => "2018-09-13T04:59:29.477"
        }
    ]
  end
end
