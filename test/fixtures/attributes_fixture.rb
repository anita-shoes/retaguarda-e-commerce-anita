class Attributes_fixture
  attr_accessor :data

  def initialize
    self.data = [
      {
          "tipoAtributo" => "Selecao",
          "isFiltro" => true,
          "nome" => "COR",
          "valor" => "BRANCO",
          "exibir" => true
      },
      {
          "tipoAtributo" => "Selecao",
          "isFiltro" => true,
          "nome" => "COR",
          "valor" => "AZUL",
          "exibir" => true
      },
      {
          "tipoAtributo" => "Selecao",
          "isFiltro" => true,
          "nome" => "TAMANHO",
          "valor" => "34",
          "exibir" => true
      },
      {
          "tipoAtributo" => "ExclusivoGoogle",
          "isFiltro" => false,
          "nome" => "GENERO_GOOGLE",
          "valor" => "male",
          "exibir" => false
      },
      {
          "tipoAtributo" => "ExclusivoGoogle",
          "isFiltro" => false,
          "nome" => "IDADE_GOOGLE",
          "valor" => "adult",
          "exibir" => false
      }
    ]
  end

end
