# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/account_activation
  def account_activation
    user = (params[:email]).nil? ? user.last : User.find_by(email: params[:email])
    user.activation_token = User.new_token
    UserMailer.account_activation(user)
  end

  def password_reset
    user = (params[:email]).nil? ? user.last : User.find_by(email: params[:email])
    user.reset_token = User.new_token
    UserMailer.password_reset(user)
  end

end
