ENV['RAILS_ENV'] = 'test'
require_relative '../config/environment'
require 'rails/test_help'
require "minitest/reporters"
require "minitest/pride"
require "minitest/mock"
require "minitest/unit"
require 'mocha/minitest'

require 'fixtures/products_fixture'
require 'fixtures/relateds_fixture'
require 'fixtures/attributes_fixture'
require 'fixtures/stocks_fixture'

require 'public_activity/testing'
PublicActivity.enabled = false

Minitest::Reporters.use!
Minitest::Unit.autorun

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  def is_logged_in?
    !session[:user_id].nil?
  end

  def log_in_as(user)
    session[:user_id] = user.id
  end
end

class ActionDispatch::IntegrationTest

  # Log in as a particular user.
  def log_in_as(user, password: 'password', remember_me: '1')
    post login_path, params: { session: { email: user.email,
                                          password: password,
                                          remember_me: remember_me } }
  end
end
