require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "login with invalid information" do
    get login_path
    assert_template 'sessions/new'
    post login_path, xhr: true, params: { session: { email: "", password: "" } }
    
    assert_response :success, @response.body
    assert_response 200
  end

  test "login with valid information" do
    products = Products_fixture.new

    @fbits = FbitsApiService.new
    @fbits.stubs(:get_products).returns(products.data)
    FbitsApiService.stubs(:new).returns(@fbits)

    get login_path
    post login_path, xhr: true, params: { session: { email:    @user.email,
                                          password: 'password' } }
    assert_response :success, @response.body
    assert_response 200

  end

  test "logout" do
    get login_path
    post login_path, params: { session: { email:    @user.email,
                                          password: 'password' } }

    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to login_url
    follow_redirect!
    assert_select "a[href=?]", logout_path, count: 0
  end

  test "go to restrict page" do
    get root_path
    assert_redirected_to login_path
    follow_redirect!
    assert_template 'sessions/new'
  end

  test "login with remembering" do
    log_in_as(@user, remember_me: '1')
    assert_not_empty cookies['remember_token']
  end

  test "login without remembering" do
    # Log in to set the cookie.
    log_in_as(@user, remember_me: '1')
    # Log in again and verify that the cookie is deleted.
    log_in_as(@user, remember_me: '0')
    assert_empty cookies['remember_token']
  end
end
