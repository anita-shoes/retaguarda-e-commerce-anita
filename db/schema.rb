# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_02_204546) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.string "trackable_type"
    t.bigint "trackable_id"
    t.string "owner_type"
    t.bigint "owner_id"
    t.string "key"
    t.text "parameters"
    t.string "recipient_type"
    t.bigint "recipient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type"
    t.index ["owner_type", "owner_id"], name: "index_activities_on_owner_type_and_owner_id"
    t.index ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type"
    t.index ["recipient_type", "recipient_id"], name: "index_activities_on_recipient_type_and_recipient_id"
    t.index ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type"
    t.index ["trackable_type", "trackable_id"], name: "index_activities_on_trackable_type_and_trackable_id"
  end

  create_table "adresses", force: :cascade do |t|
    t.string "name"
    t.string "street"
    t.string "number"
    t.string "complement"
    t.string "neighborhood"
    t.string "reference"
    t.string "city"
    t.integer "state"
    t.string "zip_code"
    t.string "country"
    t.bigint "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_adresses_on_client_id"
  end

  create_table "brands", force: :cascade do |t|
    t.string "name"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.integer "erp_id"
    t.integer "status"
    t.integer "show_menu"
    t.string "url"
    t.integer "main_category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "parent_id"
    t.index ["parent_id"], name: "index_categories_on_parent_id"
  end

  create_table "categories_products", id: false, force: :cascade do |t|
    t.bigint "product_id", null: false
    t.bigint "category_id", null: false
    t.index ["category_id", "product_id"], name: "index_categories_products_on_category_id_and_product_id"
    t.index ["product_id", "category_id"], name: "index_categories_products_on_product_id_and_category_id"
  end

  create_table "clients", force: :cascade do |t|
    t.integer "client_type"
    t.integer "origin"
    t.integer "genre"
    t.string "name"
    t.string "cpf"
    t.string "email"
    t.string "rg"
    t.string "home_phone"
    t.string "cell_phone"
    t.string "business_phone"
    t.date "birthday"
    t.string "business_name"
    t.string "cnpj"
    t.string "ie"
    t.string "responsible"
    t.string "password_digest"
    t.string "remember_digest"
    t.string "reset_digest"
    t.datetime "reset_sent_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_clients_on_email", unique: true
  end

  create_table "groups", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "order_item_attributes", force: :cascade do |t|
    t.string "name"
    t.string "value"
    t.bigint "order_item_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_item_id"], name: "index_order_item_attributes_on_order_item_id"
  end

  create_table "order_item_logs", force: :cascade do |t|
    t.integer "status"
    t.bigint "order_item_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "stock_area_id"
    t.bigint "user_id"
    t.index ["order_item_id"], name: "index_order_item_logs_on_order_item_id"
    t.index ["stock_area_id"], name: "index_order_item_logs_on_stock_area_id"
    t.index ["user_id"], name: "index_order_item_logs_on_user_id"
  end

  create_table "order_items", force: :cascade do |t|
    t.float "price"
    t.float "charged"
    t.boolean "gift"
    t.bigint "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "product_id"
    t.integer "quantity"
    t.index ["order_id"], name: "index_order_items_on_order_id"
    t.index ["product_id"], name: "index_order_items_on_product_id"
  end

  create_table "order_observations", force: :cascade do |t|
    t.text "text"
    t.boolean "internal"
    t.bigint "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_order_observations_on_order_id"
  end

  create_table "order_payment_credit_cards", force: :cascade do |t|
    t.string "number"
    t.string "name"
    t.string "expiration"
    t.string "security_code"
    t.string "document"
    t.string "token"
    t.string "info"
    t.string "flag"
    t.bigint "order_payment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_payment_id"], name: "index_order_payment_credit_cards_on_order_payment_id"
  end

  create_table "order_payment_infos", force: :cascade do |t|
    t.string "key"
    t.string "value"
    t.bigint "order_payment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_payment_id"], name: "index_order_payment_infos_on_order_payment_id"
  end

  create_table "order_payment_statuses", force: :cascade do |t|
    t.string "authorization"
    t.string "receipt"
    t.string "acquirer"
    t.bigint "order_payment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_payment_id"], name: "index_order_payment_statuses_on_order_payment_id"
  end

  create_table "order_payments", force: :cascade do |t|
    t.integer "installment"
    t.float "deduction"
    t.float "installment_value"
    t.float "interest"
    t.float "total"
    t.bigint "payment_type_id"
    t.bigint "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_order_payments_on_order_id"
    t.index ["payment_type_id"], name: "index_order_payments_on_payment_type_id"
  end

  create_table "order_shippings", force: :cascade do |t|
    t.string "address_type"
    t.string "to"
    t.string "address"
    t.string "number"
    t.string "complement"
    t.string "reference"
    t.string "zip_code"
    t.string "neighborhood"
    t.string "city"
    t.integer "state"
    t.string "country"
    t.float "estimate_value"
    t.float "charged_value"
    t.float "paid_value"
    t.integer "estimated_delivery_time"
    t.integer "given_delivery_time"
    t.integer "executed_delivery_time"
    t.bigint "shipping_type_id"
    t.bigint "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "stock_area_id"
    t.index ["order_id"], name: "index_order_shippings_on_order_id"
    t.index ["shipping_type_id"], name: "index_order_shippings_on_shipping_type_id"
    t.index ["stock_area_id"], name: "index_order_shippings_on_stock_area_id"
  end

  create_table "order_situations", force: :cascade do |t|
    t.string "name"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade do |t|
    t.float "value"
    t.float "deduction"
    t.float "internal_account"
    t.string "coupon"
    t.string "email"
    t.string "home_phone"
    t.string "cell_phone"
    t.bigint "client_id"
    t.bigint "order_situation_id"
    t.bigint "store_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_orders_on_client_id"
    t.index ["order_situation_id"], name: "index_orders_on_order_situation_id"
    t.index ["store_id"], name: "index_orders_on_store_id"
  end

  create_table "payment_types", force: :cascade do |t|
    t.string "name"
    t.boolean "status"
    t.boolean "visible"
    t.string "image"
    t.integer "interest_free_installments"
    t.integer "with_interest_installments"
    t.integer "order"
    t.float "interest_rate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_attributes", force: :cascade do |t|
    t.integer "attr_type"
    t.boolean "is_filter"
    t.string "name"
    t.string "value"
    t.boolean "show"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "product_id"
    t.index ["product_id"], name: "index_product_attributes_on_product_id"
  end

  create_table "product_images", force: :cascade do |t|
    t.string "filename"
    t.string "url"
    t.integer "order"
    t.boolean "stamp"
    t.boolean "show_thumb"
    t.integer "parent_id"
    t.integer "parent_color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_informations", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.string "information_type"
    t.boolean "show"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "product_id"
    t.index ["product_id"], name: "index_product_informations_on_product_id"
  end

  create_table "products", force: :cascade do |t|
    t.integer "group_id"
    t.string "external_parent_id"
    t.string "external_link_id"
    t.string "sku"
    t.string "ean"
    t.boolean "marketplace"
    t.string "name"
    t.string "address"
    t.boolean "show_attributes_matrix"
    t.string "author"
    t.string "editor"
    t.string "collection"
    t.integer "gender"
    t.float "cost_price"
    t.float "from_price"
    t.float "to_price"
    t.integer "delivery_deadline"
    t.boolean "is_valid"
    t.boolean "show_site"
    t.boolean "free_shipping"
    t.boolean "free_exchange"
    t.integer "weight"
    t.integer "height"
    t.integer "length"
    t.integer "width"
    t.boolean "warranty"
    t.integer "max_quantity"
    t.integer "min_quantity"
    t.integer "condition"
    t.string "video_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "brand_id"
    t.index ["brand_id"], name: "index_products_on_brand_id"
  end

  create_table "regions", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shipping_type_settings", force: :cascade do |t|
    t.string "key"
    t.string "value"
    t.bigint "shipping_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shipping_type_id"], name: "index_shipping_type_settings_on_shipping_type_id"
  end

  create_table "shipping_types", force: :cascade do |t|
    t.string "name"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stock_areas", force: :cascade do |t|
    t.string "name"
    t.integer "code"
    t.boolean "sales_status"
    t.boolean "collect_status"
    t.integer "adjustment"
    t.bigint "order_item_log_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_item_log_id"], name: "index_stock_areas_on_order_item_log_id"
  end

  create_table "stock_locations", force: :cascade do |t|
    t.string "name"
    t.integer "visible_site"
    t.integer "security"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stocks", force: :cascade do |t|
    t.integer "stock"
    t.integer "reserved"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "product_id"
    t.bigint "stock_location_id"
    t.index ["product_id"], name: "index_stocks_on_product_id"
    t.index ["stock_location_id"], name: "index_stocks_on_stock_location_id"
  end

  create_table "stores", force: :cascade do |t|
    t.string "name"
    t.integer "code"
    t.boolean "collectible"
    t.boolean "status"
    t.bigint "group_id"
    t.bigint "region_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_stores_on_group_id"
    t.index ["region_id"], name: "index_stores_on_region_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "remember_digest"
    t.string "activation_digest"
    t.boolean "activated", default: false
    t.datetime "activated_at"
    t.string "reset_digest"
    t.datetime "reset_sent_at"
    t.integer "role"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "order_item_logs", "stock_areas"
  add_foreign_key "order_item_logs", "users"
  add_foreign_key "order_items", "products"
  add_foreign_key "order_shippings", "stock_areas"
end
