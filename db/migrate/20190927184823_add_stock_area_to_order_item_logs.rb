class AddStockAreaToOrderItemLogs < ActiveRecord::Migration[5.2]
  def change
    add_reference :order_item_logs, :stock_area, foreign_key: true
    add_reference :order_item_logs, :user, foreign_key: true
  end
end
