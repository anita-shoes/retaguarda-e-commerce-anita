class CreateStockLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :stock_locations do |t|
      t.string :name
      t.integer :visible_site
      t.integer :security
      t.integer :status

      t.timestamps
    end
  end
end
