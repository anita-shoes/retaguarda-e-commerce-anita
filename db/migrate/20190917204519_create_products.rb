class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.integer :group_id
      t.string :external_parent_id
      t.string :external_link_id
      t.string :sku
      t.string :ean
      t.boolean :marketplace
      t.string :name
      t.string :address
      t.boolean :show_attributes_matrix
      t.string :author
      t.string :editor
      t.string :collection
      t.integer :gender
      t.float :cost_price
      t.float :from_price
      t.float :to_price
      t.integer :delivery_deadline
      t.boolean :is_valid
      t.boolean :show_site
      t.boolean :free_shipping
      t.boolean :free_exchange
      t.integer :weight
      t.integer :height
      t.integer :length
      t.integer :width
      t.boolean :warranty
      t.integer :max_quantity
      t.integer :min_quantity
      t.integer :condition
      t.string :video_url

      t.timestamps
      t.belongs_to :brand
    end
  end
end
