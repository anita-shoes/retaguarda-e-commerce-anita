class CreateProductImages < ActiveRecord::Migration[5.2]
  def change
    create_table :product_images do |t|
      t.string :filename
      t.string :url
      t.integer :order
      t.boolean :stamp
      t.boolean :show_thumb
      t.integer :parent_id
      t.integer :parent_color
      t.timestamps
    end
  end
end
