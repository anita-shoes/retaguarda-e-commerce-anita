class CreatePaymentTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :payment_types do |t|
      t.string :name
      t.boolean :status
      t.boolean :visible
      t.string :image
      t.integer :interest_free_installments
      t.integer :with_interest_installments
      t.integer :order
      t.float :interest_rate

      t.timestamps
    end
  end
end
