class CreateStockAreas < ActiveRecord::Migration[5.2]
  def change
    create_table :stock_areas do |t|
      t.string :name
      t.integer :code
      t.boolean :sales_status
      t.boolean :collect_status
      t.integer :adjustment
      t.belongs_to :order_item_log, :index => true

      t.timestamps
    end
  end
end
