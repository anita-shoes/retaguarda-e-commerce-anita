class CreateOrderPayments < ActiveRecord::Migration[5.2]
  def change
    create_table :order_payments do |t|
      t.integer :installment
      t.float :deduction 
      t.float :installment_value
      t.float :interest
      t.float :total
      t.belongs_to :payment_type, :index => true
      t.belongs_to :order, :index => true

      t.timestamps
    end
  end
end
