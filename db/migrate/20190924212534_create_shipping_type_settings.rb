class CreateShippingTypeSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :shipping_type_settings do |t|
      t.string :key
      t.string :value
      t.belongs_to :shipping_type, :index => true

      t.timestamps
    end
  end
end
