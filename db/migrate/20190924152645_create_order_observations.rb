class CreateOrderObservations < ActiveRecord::Migration[5.2]
  def change
    create_table :order_observations do |t|
      t.text :text
      t.boolean :internal
      t.belongs_to :order, :index => true

      t.timestamps
    end
  end
end
