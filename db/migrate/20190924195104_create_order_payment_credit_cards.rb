class CreateOrderPaymentCreditCards < ActiveRecord::Migration[5.2]
  def change
    create_table :order_payment_credit_cards do |t|
      t.string :number
      t.string :name
      t.string :expiration
      t.string :security_code
      t.string :document
      t.string :token
      t.string :info
      t.string :flag
      t.belongs_to :order_payment, :index => true

      t.timestamps
    end
  end
end
