class CreateOrderShippings < ActiveRecord::Migration[5.2]
  def change
    create_table :order_shippings do |t|
      t.string :address_type
      t.string :to
      t.string :address
      t.string :number
      t.string :complement
      t.string :reference
      t.string :zip_code
      t.string :neighborhood
      t.string :city
      t.integer :state
      t.string :country
      t.float :estimate_value
      t.float :charged_value
      t.float :paid_value
      t.integer :estimated_delivery_time
      t.integer :given_delivery_time
      t.integer :executed_delivery_time
      t.belongs_to :shipping_type, :index => true
      t.belongs_to :order, :index => true

      t.timestamps
    end
  end
end
