class CreateProductAttributes < ActiveRecord::Migration[5.2]
  def change
    create_table :product_attributes do |t|
      t.integer :attr_type
      t.boolean :is_filter
      t.string :name
      t.string :value
      t.boolean :show

      t.timestamps
      t.belongs_to :product
    end
  end
end
