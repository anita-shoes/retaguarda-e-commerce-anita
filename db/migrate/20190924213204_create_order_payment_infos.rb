class CreateOrderPaymentInfos < ActiveRecord::Migration[5.2]
  def change
    create_table :order_payment_infos do |t|
      t.string :key
      t.string :value
      t.belongs_to :order_payment, :index => true

      t.timestamps
    end
  end
end
