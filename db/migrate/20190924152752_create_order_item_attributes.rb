class CreateOrderItemAttributes < ActiveRecord::Migration[5.2]
  def change
    create_table :order_item_attributes do |t|
      t.string :name
      t.string :value
      t.belongs_to :order_item, :index => true

      t.timestamps
    end
  end
end
