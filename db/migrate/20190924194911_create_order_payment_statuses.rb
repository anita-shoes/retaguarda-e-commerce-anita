class CreateOrderPaymentStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :order_payment_statuses do |t|
      t.string :authorization
      t.string :receipt
      t.string :acquirer
      t.belongs_to :order_payment, :index => true

      t.timestamps
    end
  end
end
