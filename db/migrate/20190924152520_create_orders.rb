class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.float :value
      t.float :deduction
      t.float :internal_account
      t.string :coupon
      t.string :email
      t.string :home_phone
      t.string :cell_phone
      t.belongs_to :client, :index => true
      t.belongs_to :order_situation, :index => true
      t.belongs_to :store, :index => true

      t.timestamps
    end
  end
end
