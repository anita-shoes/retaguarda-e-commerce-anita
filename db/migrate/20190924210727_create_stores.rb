class CreateStores < ActiveRecord::Migration[5.2]
  def change
    create_table :stores do |t|
      t.string :name
      t.integer :code
      t.boolean :collectible
      t.boolean :status
      t.belongs_to :group, :index => true
      t.belongs_to :region, :index => true

      t.timestamps
    end
  end
end
