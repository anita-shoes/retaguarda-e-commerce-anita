class CreateOrderItemLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :order_item_logs do |t|
      t.integer :status
      t.belongs_to :order_item, :index => true

      t.timestamps
    end
  end
end
