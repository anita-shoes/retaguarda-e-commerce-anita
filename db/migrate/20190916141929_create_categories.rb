class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :name
      t.integer :erp_id
      t.integer :status
      t.integer :show_menu
      t.string :url
      t.integer :main_category

      t.timestamps
    end

    add_reference :categories, :parent, index: true
  end
end
