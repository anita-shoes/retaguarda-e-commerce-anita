class CreateClients < ActiveRecord::Migration[5.2]
  def change
    create_table :clients do |t|
      t.integer :type
      t.integer :origin
      t.integer :genre
      t.string :name
      t.string :cpf
      t.string :email
      t.string :rg
      t.string :home_phone
      t.string :cell_phone
      t.string :business_phone
      t.date :birthday
      t.string :business_name
      t.string :cnpj
      t.string :ie
      t.string :responsible
      t.string :password_digest
      t.string :remember_digest
      t.string :reset_digest
      t.datetime :reset_sent_at

      t.timestamps
    end
    
    add_index :clients, :email, unique: true
  end
end
