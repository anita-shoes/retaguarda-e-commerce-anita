class CreateAdresses < ActiveRecord::Migration[5.2]
  def change
    create_table :adresses do |t|
      t.string :name
      t.string :street
      t.string :number
      t.string :complement
      t.string :neighborhood
      t.string :reference
      t.string :city
      t.integer :state
      t.string :zip_code
      t.string :country
      t.belongs_to :client, :index => true

      t.timestamps
    end
  end
end
