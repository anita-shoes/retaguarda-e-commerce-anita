class CreateProductInformations < ActiveRecord::Migration[5.2]
  def change
    create_table :product_informations do |t|
      t.string :title
      t.text :text
      t.string :information_type
      t.boolean :show

      t.timestamps
      t.belongs_to :product
    end
  end
end
