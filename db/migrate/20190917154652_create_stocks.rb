class CreateStocks < ActiveRecord::Migration[5.2]
  def change
    create_table :stocks do |t|
      t.integer :stock
      t.integer :reserved

      t.timestamps
      t.belongs_to :product
      t.belongs_to :stock_location
    end
  end
end
