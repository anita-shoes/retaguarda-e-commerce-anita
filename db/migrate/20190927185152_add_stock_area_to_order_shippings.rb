class AddStockAreaToOrderShippings < ActiveRecord::Migration[5.2]
  def change
    add_reference :order_shippings, :stock_area, foreign_key: true
  end
end
