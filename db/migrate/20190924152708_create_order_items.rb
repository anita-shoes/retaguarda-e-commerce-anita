class CreateOrderItems < ActiveRecord::Migration[5.2]
  def change
    create_table :order_items do |t|
      t.float :price
      t.float :charged
      t.boolean :gift
      t.belongs_to :order, :index => true

      t.timestamps
    end
  end
end
