class RenameTypeToClientType < ActiveRecord::Migration[5.2]
  def change
  	rename_column :clients, :type, :client_type
  end
end
