class SaciWsService < HttpService
  attr_accessor :product_ws

  require 'savon'

  def get_product_ws
    self.product_ws = Rails.application.config.saci_api_url #self.get_ws("http://192.168.100.3/saciWSVirtual/produtows.php?wsdl")

  end

  def get_ws(address)
    Savon.client(
      wsdl:        address,
      logger:      Rails.logger,
      convert_request_keys_to: :none,
      log_level:   :debug,
      log:         true,
      ssl_verify_mode: :none
    )

  end

  def get_product(code)
    self.get_product_ws if self.product_ws.nil?

    product = self.product_ws.call(:listar, message: {crypt: 186, dados: '<dados>
          <codigo_produto>'+code+'</codigo_produto>
          <nome></nome>
          <referencia_fabricante></referencia_fabricante>
          </dados>'}
        )
    Hash.from_xml(product.body[:listar_response][:return])

  end

end
