class HttpService
  require 'net/http'
  require 'uri'

  attr_accessor :uri, :http, :request, :response, :data_method, :uri_path, :params, :headers, :body

  def set_uri(uri)
    self.uri = URI.parse(uri)
  end

  def set_requestGet
    self.request = Net::HTTP::Get.new(self.uri.path.to_s + '?' + self.uri.query.to_s)
  end

  def set_requestPost
    self.http = Net::HTTP.new(self.uri.host, self.uri.port)
    self.http.use_ssl = true
    self.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    self.headers.merge!('Content-Type': 'text/json')
    self.request = Net::HTTP::Post.new(uri.request_uri, self.headers)
  end

  def set_requestDelete
    self.http = Net::HTTP.new(self.uri.host, self.uri.port)
    self.http.use_ssl = true
    self.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    self.headers.merge!('Content-Type': 'text/json')
    self.request = Net::HTTP::Delete.new(uri.request_uri, self.headers)
  end

  def set_requestPut
    self.http = Net::HTTP.new(self.uri.host, self.uri.port)
    self.http.use_ssl = true
    self.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    self.headers.merge!('Content-Type': 'text/json')
    self.request = Net::HTTP::Put.new(uri.request_uri, self.headers)
  end

  def set_header(k, v)
    self.request[k] = v
  end

  def set_request_content_type(content_type)
    self.request.content_type = content_type
  end

  def set_response_GET
    self.response = Net::HTTP.start(self.uri.host, self.uri.port, :use_ssl =>  uri.scheme == 'https')  do |http|
      self.response = http.request(self.request)
    end
  end

  def set_response_POST
    self.response = self.http.request(self.request)
  end

  def set_response_PUT
    set_response_POST
  end

  def set_response_DELETE
    set_response_POST
  end

  def do_request
    return self.return_utf8(self.response.request(self.request).body)
  end

  def json_utf8(response)
    return ActiveSupport::JSON.decode(response.force_encoding('UTF-8'))
  end

end
