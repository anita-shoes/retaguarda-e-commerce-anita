class FbitsApiService < HttpService

  attr_accessor :campos_adicionais, :uri

  def initialize
    self.uri = Rails.application.config.fbits_api_uri
    self.headers = { Authorization: Rails.application.config.token }
    self.params = Hash.new
    self.campos_adicionais = Array.new
    self.uri_path.to_s
  end

  def prepare_GET
    self.set_uri mount_url
    self.set_requestGet
    self.set_request_content_type 'application/json'

    self.headers.each do |k, v|
      self.set_header(k,v)
    end
  end

  def prepare_POST
    self.set_uri mount_url
    self.set_requestPost
    self.request.body = self.body
  end

  def prepare_PUT
    self.set_uri mount_url
    self.set_requestPut
    self.request.body = self.body
  end

  def prepare_DELETE
    self.set_uri mount_url
    self.set_requestDelete
  end

  def post_product(product)
    self.data_method = 'POST'
    self.uri_path = 'produtos'

    self.body = product.to_json
    return self.go
  end

  def post_category(category)
    self.data_method = 'POST'
    self.uri_path = 'categorias'
    self.body = category.to_json
    return self.go
  end

  def post_product_category(id, ids_list, main_category=0, field="Sku")
    self.data_method = 'POST'
    self.uri_path = "produtos/#{id}/categorias"
    self.params[:tipoIdentificador] = field

    self.body = {
      categoriaPrincipalId: ids_list[main_category],
      listaCategoriaId: ids_list
    }.to_json
    return self.go
  end

  def put_product(id, product, field="Sku")
    self.data_method = 'PUT'
    self.uri_path = "produtos/#{id}"
    self.params[:tipoIdentificador] = field

    self.body = product.to_json
    return self.go
  end

  def put_stock(json, field="Sku")
    self.data_method = 'PUT'
    self.uri_path = "produtos/estoques"
    self.params[:tipoIdentificador] = field

    self.body = json.to_json
    return self.go
  end

  def put_prices(json, field="Sku")
    self.data_method = 'PUT'
    self.uri_path = "produtos/precos"
    self.params[:tipoIdentificador] = field

    self.body = json.to_json
    return self.go
  end

  def post_product_images(id, image_list, field="Sku")
    self.data_method = 'POST'
    self.uri_path = "produtos/#{id}/imagens"
    self.params[:tipoIdentificador] = field

    self.body = image_list.to_json
    return self.go
  end

  def post_brand(brand)
    self.data_method = 'POST'
    self.uri_path = 'fabricantes'

    self.body = brand.to_json
    return self.go
  end

  def get_product_categories(id, field="Sku")
    self.data_method = 'GET'
    self.uri_path = "produtos/#{id}/categorias"
    self.params[:tipoIdentificador] = field

    return self.go
  end

  def get_product_images(id, field="Sku")
    self.data_method = 'GET'
    self.uri_path = "produtos/#{id}/imagens"
    self.params[:tipoIdentificador] = field

    result = self.go
    (self.go.nil?) ? [] : result
  end

  def delete_product_category(sku, category, field="Sku")
    self.data_method = 'DELETE'
    self.uri_path = "produtos/#{sku}/categorias/#{category}"
    self.params[:tipoIdentificador] = field
    return self.go
  end

  def delete_category(id)
    self.data_method = 'DELETE'
    self.uri_path = "categorias/#{id}"

    return self.go
  end

  def delete_product_image(sku, image, field="Sku")
    self.data_method = 'DELETE'
    self.uri_path = "produtos/#{sku}/imagens/#{image}"
    self.params[:tipoIdentificador] = field

    return self.go
  end

  def get_relacionados(id, field)
    self.data_method = 'GET'
    self.uri_path = 'produtos/' + id + '/relacionados'
    self.params[:tipoIdentificador] = field

    return self.go
  end

  def get_client_adresses(client_id)
    self.data_method = 'GET'
    self.uri_path = "usuarios/#{client_id}/enderecos" 

    return self.go
  end

  def get_brands
    self.data_method = 'GET'
    self.uri_path = 'fabricantes'

    return self.go
  end

  def get_categories
    self.data_method = 'GET'
    self.uri_path = 'categorias'

    return self.go
  end

  def get_product_categories(sku)
    self.data_method = 'GET'
    self.uri_path = "produtos/#{sku}/categorias"

    self.params[:tipoIdentificador] = 'sku'
    return self.go
  end

  def get_category_erp(erp_id)
    self.data_method = 'GET'
    self.uri_path = "categorias/erp/#{erp_id}"

    return self.go
  end

  def get_category(id)
    self.data_method = 'GET'
    self.uri_path = "categorias/#{id}"
    
    return self.go
  end

  def get_clients(page=0, size=50, filters={})
    self.data_method = 'GET'
    self.uri_path = 'usuarios'

    self.params[:pagina] = page
    self.params[:quantidadeRegistros] = size

    return self.go
  end

  def get_products(page=0, size=50)
    self.data_method = 'GET'
    self.uri_path = 'produtos'

    self.params[:pagina] = page
    self.params[:quantidadeRegistros] = size
    self.campos_adicionais = ['Estoque', 'Atributo', 'Informacao']

    return self.go
  end

  def get_orders(page=0, size=50)
    self.data_method = 'GET'
    self.uri_path = 'pedidos'

    self.params[:pagina] = page
    self.params[:quantidadeRegistros] = size
    self.params[:dataInicial] = '2000-01-01'
    self.params[:dataFinal] = '2019-12-31'

    return self.go
  end

  def get_product(id, field)
    self.data_method = 'GET'
    self.uri_path = 'produtos/' + id
    self.campos_adicionais = ['Estoque', 'Atributo']
    self.params[:tipoIdentificador] = field

    return self.go
  end

  def base_path
    Rails.application.config.fbits_api_uri + self.uri_path
  end

  def mount_url
    url = base_path + '?env=prod'
    url += '&pagina=' + self.params[:pagina].to_s if !self.params[:pagina].nil?
    url += '&categorias=' + self.params[:categorias].to_s if !self.params[:categorias].nil?
    url += '&fabricantes=' + self.params[:fabricantes].to_s  if !self.params[:fabricantes].nil?
    url += '&quantidadeRegistros=' + self.params[:quantidadeRegistros].to_s if !self.params[:quantidadeRegistros].nil?
    url += '&tipoIdentificador=' + self.params[:tipoIdentificador].to_s if !self.params[:tipoIdentificador].nil?
    url += '&dataInicial=' + self.params[:dataInicial].to_s if !self.params[:dataInicial].nil?
    url += '&dataFinal=' + self.params[:dataFinal].to_s if !self.params[:dataFinal].nil?

    self.campos_adicionais.each do | campo_adicional |
      url += '&camposAdicionais=' + campo_adicional
    end
    url
  end

  def go
    self.send("prepare_#{self.data_method}")

    code = "429"
    while code == "429" do
      self.send("set_response_#{self.data_method}")
      code = self.response.code

      sleep(10.seconds) if code == "429"
    end

    return !(self.response.body).try(:empty?) ? self.json_utf8(self.response.body) : true
  end
end
