module PicturesSwapHelper

  def get_folder_content(sub_folder)
    content = Dir["#{Rails.application.config.pictures_swap}#{sub_folder}*"]
    content.sort
  end

  def pre_creation_list
    collection = {}
    list_codes.each do |code|
      collection[code] = {}
      list_colors(code).each do |color|
        collection[code][color] = {}
      end
    end
    dbg('Pre cration list', collection, :green, nil, :bold)
    collection
  end

  def get_last_node_content(sub_folder)
    folders = get_folder_content(sub_folder)
    last_node(folders)
  end

  def list_codes
    codes = get_last_node_content("")
  end

  def list_pictures(code, color)
    get_folder_content("#{code}/#{color}/")
  end

  def list_colors(code)
    get_last_node_content("#{code}/")
  end

  def last_node(folders)
    codes = []
    folders.each do |folder|
      codes << folder.split("/").last
    end
    codes
  end

  def to_review(code)
    base_from = Rails.application.config.pictures_swap
    base_to = Rails.application.config.pictures_review

    FileUtils.copy_entry(base_from+code, base_to+code)
    FileUtils.rm_rf(base_from+code)
    dbg('Product upload OK 🙌👏✌️', code, :green, :nil, :bold)
  end

end
