module ApplicationHelper

  def dbg(description, to_debug, color=nil, background=nil, mode=nil)
    puts "\n"
    puts "<><><><><><><><><><>".swap * 10
    puts "\n#{description.bold}:\n"
    puts to_debug.to_s.colorize(color: color, background: background, mode: mode)
    puts "\n"
    puts "\n"
  end

  def connect_nerus
    connect_db("nerus_#{ENV["RAILS_ENV"]}")
  end

  def connect_retaguarda
    connect_db(ENV["RAILS_ENV"])
  end

  def connect_db(db)
    ActiveRecord::Base.establish_connection(db.to_sym)
  end

  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "Retaguarda Anita Shoes"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  def fa(fa_class, fa_style="fas", style="margin-right:7px;")
  	"<i style='#{style}' class='#{fa_style} #{fa_class}'></i>".html_safe
  end

  def add_migalha(link, label)

  end
end
