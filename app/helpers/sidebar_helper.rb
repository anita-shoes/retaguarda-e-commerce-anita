module SidebarHelper

  def show_group?(group)
    groups = get_group_links
    groups[group.to_sym].each do |group|
	    	group.second.each do |group_params|
	    		return true if (group_params[:controller] == params[:controller] && group_params[:action] == params[:action])
	    	end
	    end
	  false
  end

  def active_link?(controller, action)
		if (params[:controller] == controller && params[:action] == action)
			true 
		else
			false
		end
  end

  def get_group_links
  	{
  		:configurations => {
  			:users => [
  				{:controller => 'users', :action => 'index'},
  				{:controller => 'users', :action => 'show'},
  				{:controller => 'users', :action => 'new'},
  				{:controller => 'users', :action => 'edit'}
  			],
  			:activities => [
  				{:controller => 'activities', :action => 'index'}
  			]
  		},
      :ecommerce => {
        :products => [
          {:controller => 'products', :action => 'index'},
          {:controller => 'products', :action => 'show'},
          {:controller => 'products', :action => 'new'},
          {:controller => 'products', :action => 'edit'}
        ],
        :clients => [
          {:controller => 'clients', :action => 'index'},
          {:controller => 'clients', :action => 'show'},
          {:controller => 'clients', :action => 'new'},
          {:controller => 'clients', :action => 'edit'}
        ],
        :orders => [
          {:controller => 'orders', :action => 'index'},
          {:controller => 'orders', :action => 'show'},
          {:controller => 'orders', :action => 'new'},
          {:controller => 'orders', :action => 'edit'}
        ]
      }
  	}
  end

end