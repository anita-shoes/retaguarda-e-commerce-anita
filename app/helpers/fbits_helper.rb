module FbitsHelper

  def cleanup_categories(sku)
    categories = api.get_product_categories(sku)
    delete_product_categories(sku, categories) if categories.count > 0 && categories.is_a?(Array)
  end

  def cleanup_images(sku)
    images = api.get_product_images(sku)
    delete_product_images(sku, images) if images.count > 0 && images.is_a?(Array)
  end

  def has_product(identifier, identifier_type="Sku")
    check = api.get_product(identifier, identifier_type)
    (check["produtoId"]) ? true : false
  end

  def edit_product(product)
    api.put_product(product.sku, product)
    dbg("Product UPDATED from Fbits product", "#{product.idPaiExterno} / #{product.sku}", :yellow, nil, :bold)
  end

  def create_product(product)
    fails = [0, 1510]
    status = 0
    while fails.include?(status)
      response = api.post_product(product)
      status = (response.is_a?(Hash)) ? response["codigo"] : 200

      if status == 1510
        api.post_brand(BrandFbits.new(product.fabricante)) 
        dbg("Brand CREATED", product.fabricante, :green, nil, :bold)
      end
    end 
    dbg("Product CREATED", "#{product.idPaiExterno} / #{product.sku}", :green, nil, :bold)
    response
  end

  def create_product_categories(sku, categories)
    parent = "0"
    url_nodes = []
    ids_list = []
    categories.each do |category|
      url_nodes << category[:name].downcase.sub(" ", "-")
      category_id = create_or_get_category_id(category, parent, url_nodes.join("/"))
      ids_list << category_id
      parent = category_id
    end
    api.post_product_category(sku, ids_list)
    dbg("Categories ADD to product", "#{sku}: #{ids_list.join(', ')}", :green, nil, :bold)
  end

  def create_or_get_category_id(category, parent_id, url)
    real_category = existing_category(category)
    if real_category.nil?
      parent = api.get_category(parent_id)
      parent_id = (parent) ? parent["id"] : 0

      category_fbits = CategoryFbits.new(category, parent_id, url)
      category_id = api.post_category(category_fbits)
      @category_list = nil
      dbg("Category CREATED", "#{category[:name]} / #{parent_id}", :green, nil, :bold)
    else
      category_id = real_category["id"] 
    end

    category_id
  end

  def update_stock(json)
    result = api.put_stock(json)
    dbg('Stock UPDATED 😎', "...", :nil, :nil, :blink)
  end

  def update_price(json)
    result = api.put_prices(json)
    dbg('Prices UPDATED 😎', "...", :nil, :nil, :blink)
  end

  def existing_category(category)
    @category_list = api.get_categories if !defined?(@category_list) || @category_list.nil?
    @category_list.each do |fbits_category|
      return fbits_category if fbits_category["categoriaERPId"] == category[:id].to_s
    end
    nil
  end

  def delete_category(id)
    api.delete_category(id)
  end

  def delete_product_categories (sku, categories)
    categories.each do |category|
      api.delete_product_category(sku, category["id"])
    end
    dbg('Categories DELETED from Fbits product', categories.count, :red, :white, :bold)
  end

  def create_product_images (sku, images)
    dbg('UPLOADIG images...  ⏳', "#{sku}", :green, :nil, :blink)
    images.each_with_index do |image, key|
      base64 = Base64.encode64(File.read(image))
      image_list = [ImageFbits.new(base64, key+1)]
      api.post_product_images(sku, image_list)
      dbg("Product image CREATED", "#{sku}: #{key+1}", :green, nil, :bold)
    end
  end

  def delete_product_images (sku, images)
    images.each do |image|
      api.delete_product_image(sku, image["idImagem"])
    end
    dbg('Iamges DELETED from Fbits product', images.count, :red, :white, :bold)
  end

  def api
    FbitsApiService.new
  end
end
