module UsersHelper

  def gravatar_for(user, size=80, extra_class=nil, extra_style="")
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}&d=http://adm.anita.com.br/default-profile-icon.png"
    image_tag(gravatar_url, alt: user.name, class: "gravatar #{extra_class}", style: extra_style)
  end

end