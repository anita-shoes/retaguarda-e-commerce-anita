module NerusHelper

  def run(query)
    connect_nerus
    result = ActiveRecord::Base.connection.execute(query)
    connect_retaguarda

    result
  end

  def run_method(method_name, params)
    self.send(method_name, params)
  end

  def nerus_grids(collection)
    collection.each do | item |
      add_grid_query_filter(item.first, item.second)
    end
    grids = labeled(run(grids_query), grids_query_labels)
    dbg('Total SKUs Selected from NERUS', grids.count, :green, :nil, :blink)
    
    return map_collection(collection, grids)
  end

  def map_collection(collection, grids)
    grids.each do |grid|
      collection[grid[:product_number]][grid[:color_code]].merge!(grid[:sku] => grid) if collection[grid[:product_number]] && collection[grid[:product_number]][grid[:color_code]]
    end
    collection
  end

  def get_grid_query_filter(code, colors)
    "T1.prdno = '#{code}' AND REPLACE(substr(T1.grade, 1, 6), '.', '') IN ('#{colors.keys.join("','")}')"
  end

  def add_grid_query_filter(code, colors)
    (@grid_query_filters ||= []) << get_grid_query_filter(code, colors)
  end

  def nerus_stock(code, color)
    skus = labeled(run(stock_query(code, color)), stock_query_labels)
    dbg("Total SKUs Selected from #{code}", skus.count, :green, :nil, nil)
    
    return skus
  end

  def stock_query(code, color)
    query = "SELECT TRIM(T3.barcode) AS sku,
                    REPLACE(substr(T1.grade, 1, 6), '.', '') AS color_code,
                    GROUP_CONCAT(CONCAT(T1.storeno, ':', ROUND((T1.qtty_varejo+T1.qtty_atacado)/1000))) AS grid
                    FROM stk AS T1
          LEFT JOIN prdbar AS T3 ON (T1.prdno=T3.prdno AND T1.grade=T3.grade)
              WHERE T1.storeno in (10,12,13,14,19,22,26,27)
                AND T3.prdno = #{code}
                AND REPLACE(substr(T1.grade, 1, 6), '.', '') = '#{color}'
           GROUP BY 1;"
    query
  end

  def grids_query
    query = "SELECT TRIM(T3.barcode) AS sku,
             TRIM(T1.prdno) AS product_number,
             REPLACE(substr(T1.grade, 1, 6), '.', '') AS color_code,
             SUBSTR(T1.grade, 7) AS 'size',
             prdnam.name AS 'name',
             GROUP_CONCAT(CONCAT(T1.storeno, ':', ROUND((T1.qtty_varejo+T1.qtty_atacado)/1000))) AS grid,
             grdnam.name AS color_attribute,
             IF(
               vend.no IN(18,30,36,38,90,91,107,132,159,413,415,486,741,745,755,898,937,967,1148,1294,1314,1389,1492,1628,1634,1713,1733,2044,2068,2105,2118,2265,2601,2616,2638,2698,2736,2797,2862,2864,2874,2898,2904,2905,2913,2944,2959,3031,3104,3180,3456,3616,3627,3714,3715,3756,3814,4133,4307,4466,4931,4990,5626,5781,5841,5990,6562,6928,7523,8874,9386,9407,9659,9827,10003,10980,12238,15132,15659,16230,17072,18104,18395,18397,18675,18775,19031,21182,21224,22833,25302,25837,39372,39780,41134,41321,57333,60066,60134,61753,63907,65515,67783,73987,75953,76033,78945,79288,80713,81257,82173,82298,82299,82303,82831,82913,83638,91579,92522,92523,92524,92525,92700,93187,93737), 
               'Ferrette', 
               vend.sname
              ) AS brand_name,
             T2.sp/100 AS initial_price,
             if(d.refprice > 0, d.refprice/100, d1.refprice/100) AS final_price,
             grupo.no AS parente_category_id,
             grupo.name AS parent_category,
             deptno.no AS child_category_id,
             IF(INSTR(deptno.name, ' - '), SUBSTR(deptno.name, INSTR(deptno.name, ' - ')+ 3), deptno.name) AS child_category,
             cl.no AS grand_child_category_id,
             IF(INSTR(cl.name, ' - '), SUBSTR(cl.name, INSTR(cl.name, ' - ')+ 3), cl.name) AS grand_child_category
        FROM stk AS T1
   LEFT JOIN prd AS T2 ON (T1.prdno  = T2.no)
             LEFT JOIN prdbar AS T3 ON (T1.prdno=T3.prdno AND T1.grade=T3.grade)
             LEFT JOIN prp AS d ON(T1.prdno=d.prdno and d.storeno=10)
             LEFT JOIN grdnam ON (grdnam.valno = MID(T1.grade,1,6))
             LEFT JOIN prdnam ON (T2.no=prdnam.prdno)
             LEFT JOIN cl AS grupo ON (grupo.no=T2.groupno)
             LEFT JOIN cl AS deptno ON (deptno.no=T2.deptno)
             LEFT JOIN cl AS cl ON (cl.no=T2.clno)
             LEFT JOIN vend ON (vend.no=T2.mfno)
             LEFT JOIN prp AS d1 ON(T1.prdno=d1.prdno and d1.storeno=36)
       WHERE T2.groupno not in (900000,990000,910000) 
         AND T1.storeno in (10,12,13,14,19,22,26,27)
         AND (#{@grid_query_filters.join("\n OR ")})
    GROUP BY 1
    ORDER BY 2,3,4"
    query
  end

  def labeled(results, labels)
    labeled_results = []
    results.each do |result|
      labeled_column = Hash.new
      result.each_with_index do |column, key|
        labeled_column[labels[key]] = column
      end
      labeled_results << labeled_column
    end
    
    labeled_results
  end

  def grid_categories(grid)
    [
      {id: grid[:parent_category_id], name: grid[:parent_category]},
      {id: grid[:child_category_id], name: grid[:child_category]},
      {id: grid[:grand_child_category_id], name: grid[:grand_child_category]}
    ]
  end

  def grids_query_labels 
    [
      :sku,
      :product_number,
      :color_code,
      :size,
      :name,
      :grid,
      :color_attribute,
      :brand_name,
      :initial_price,
      :final_price,
      :parent_category_id,
      :parent_category,
      :child_category_id,
      :child_category,
      :grand_child_category_id,
      :grand_child_category
    ]
  end

  def stock_query_labels 
    [
      :sku,
      :color_code,
      :grid
    ]
  end

end
