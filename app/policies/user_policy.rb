class UserPolicy < ApplicationPolicy
  def update?
    @record.id == @user.id or @user.super?
  end

  def destroy?
    @user.super? && @record.id != @user.id
  end

  def permitted_attributes
    if update?
      [:name, :email, :password, :password_confirmation]
    else
      []
    end
  end

end