class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@anita.com.br'
  layout 'mailer'
end
