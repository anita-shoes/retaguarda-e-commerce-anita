class LogsController < ApplicationController
	
  def index
     @logs = PublicActivity::Activity.paginate(page: params[:page])
  end

end
