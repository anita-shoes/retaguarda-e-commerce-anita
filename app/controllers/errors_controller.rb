class ErrorsController < ApplicationController
  skip_before_action :login_required!

  def not_found
    redirect_to "https://www.instagram.com/anitaonline/"  
  end

  def unacceptable
    respond_to do |format|
      format.html { render status: 422 }
    end
  end

  def internal_error
    respond_to do |format|
      format.html { render status: 500 }
    end
  end
end