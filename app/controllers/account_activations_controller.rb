class AccountActivationsController < ApplicationController
	skip_before_action :login_required!
	def edit
	  user = User.find_by(email: params[:email])
	  if user && !user.activated? && user.authenticated?(:activation, params[:id])
	    user.activate
	    log_in user
	    flash[:hello] = {
        :type => 'success',
        :title => '\o/',
        :msg => 'Sua conta está ativada. Agora é mão na massa!'
      }
	    redirect_to user
	  else
	    flash[:warning] = {
        :type => 'error',
        :title => 'Ops',
        :msg => 'Link de ativação inválido. Entre em contato com a administração.'
      }
	    redirect_to root_url
	  end
	end
end