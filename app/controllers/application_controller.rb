class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  include PublicActivity::StoreController 
  include Pundit
  require 'colorize'
  include SessionsHelper
  include ApplicationHelper

  before_action :set_header
  before_action :set_locale
  before_action :login_required!
  protect_from_forgery with: :exception

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  
  def home
  end
  
  def get_current_user
    current_user
  end

  protected
    def set_header
      response.set_header('Referrer-Policy', 'origin')
    end

    def login_required!
      if !logged_in?
        store_location
        
        flash[:warning] = {
          :type => 'error',
          :title => 'Você não está logado',
          :msg => 'Faça o login para acessar esta página e confirme se você tem o devido acesso liberado. Em caso de dúvidas entre em contato com o suporte.'
        } if params[:controller] != "application" and params[:action] != "home"
        redirect_to login_path
      end
    end

    def set_locale
      locale = params[:locale] || cookies[:locale]
      if locale.present?
        I18n.locale = locale
        cookies[:locale] = { value: locale, expires: 30.days.from_now}
      end
    end

    def user_not_authorized
      flash[:warning] = {
        :type => 'error',
        :title => 'Ops, você não é bem-vindo nesta página',
        :msg => 'Cada usuário é responsável pelo controle dos seus dados.'
      }
      redirect_to(request.referrer || root_path)
    end

end
 