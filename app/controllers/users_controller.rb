class UsersController < ApplicationController
	skip_before_action :login_required!, only: [:new, :create]
  layout :resolve_layout

  def index
     @users = User.paginate(page: params[:page], per_page: 10)
  end

  def show
    @user = User.find(params[:id])
  end

  def new
  	@user = User.new
  end

  def my_profile
    @user = current_user
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:new] = {
        :type => 'success',
        :title => 'Quase lá!',
        :msg => 'Verifique seu e-mail para ativar sua conta.'
      }
      
    	redirect_to login_path + "?nub=1"
    else
			@model = @user
			respond_to do |format|  
      	format.js { render "/shared/show_form_errors" }
      end

    end
  end

  def edit
    @user = authorize User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    authorize @user

    if @user.update(post_params)
      flash[:edit] = {
        :type => 'success',
        :title => 'Ok',
        :msg => 'Seus dados foram editados com sucesso.'
      }
      redirect_to @user
    else
      @model = @user
      respond_to do |format|  
        format.js { render "/shared/show_form_errors" }
      end
    end
  end

  def destroy
    @user = User.find(params[:id])
    authorize @user

    if @user.destroy
      flash[:destroy] = {
        :type => 'success',
        :title => 'Ok',
        :msg => 'Usuário deletado. Hasta la vista, baby!'
      }
    else
     flash[:destroy] = {
        :type => 'error',
        :title => 'Erro',
        :msg => 'Não foi possível deletar, tem certeza que você pode fazer isso?'
      }
    end
    redirect_to users_url
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end

  	def resolve_layout
      case action_name
      	when "new", "create"
      	  "clean"
    	else
          "application"
    	end
  	end

    def post_params
      params.require(:user).permit(policy(@user).permitted_attributes)
    end

end
