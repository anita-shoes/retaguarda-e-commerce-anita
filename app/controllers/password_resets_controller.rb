class PasswordResetsController < ApplicationController
	skip_before_action :login_required!
	# before_action :get_user,   only: [:edit, :update]
 #  before_action :valid_user, only: [:edit, :update]
	
	layout 'clean'

  def new
  end

  def create
  	@user = User.find_by(email: params[:password_reset][:email].downcase)
    if @user
      @user.create_reset_digest
      @user.send_password_reset_email
      flash[:info] = {
	      :type => 'success',
	      :title => 'Quase lá...',
	      :msg => 'Enviamos um e-mail com as instruções para recuperação de sua senha.'
	    }
      redirect_to root_url
    else
      flash[:warning] = {
        :type => 'error',
        :title => 'Ops!',
        :msg => 'Não encontramos este e-mail. Se ele é seu, solicite um novo acesso.'
      }
      redirect_to new_password_reset_url
    end
  end

  def edit
  	get_valid_user
  end

  def update
  	get_valid_user
  	if params[:user][:password].empty?
      @user.errors.add(:password, "Não pode ser em branco, claro!")

      exit_form_errors
    elsif @user.update_attributes(user_params)
      log_in @user
      flash[:info] = {
	      :type => 'success',
	      :title => 'Pronto',
	      :msg => 'Sua senha foi recuperada, cuide melhor dela, ok?.'
	    }
      redirect_to root_url
    else
      exit_form_errors
    end
  end

  def get_valid_user
  	get_user
  	valid_user
  	check_expiration
  end

  def exit_form_errors
  	@model = @user
  	respond_to do |format|
    	format.js { render "/shared/show_form_errors" }
    end
  end

  private
   	def user_params
      params.require(:user).permit(:password, :password_confirmation)
    end

  	def get_user
      @user = User.find_by(email: params[:email])
    end

    def valid_user
      unless (@user && @user.activated? &&
              @user.authenticated?(:reset, params[:id]))
	      
	      flash[:warning] = {
	        :type => 'error',
	        :title => 'Alto lá',
	        :msg => 'E-mail não identificado para esta operação.'
	      }
        redirect_to root_url
      end
    end

    def check_expiration
      if @user and @user.password_reset_expired?
        flash[:warning] = {
	        :type => 'error',
	        :title => 'Recomece',
	        :msg => 'O prazo pra reiniciar a senha expirou. Solicite a recuperação novamente.'
	      }
        redirect_to new_password_reset_url
      end
    end
end
#password_resets/yEFNj_g33_1WCVBZ3UAFIg/edit?email=site%40anita.com.br