class SessionsController < ActionController::Base
  layout 'clean'

  include SessionsHelper

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        flash[:success] = {
          :type => 'success',
          :title => 'Bem-vindo',
          :msg => "Hora de colocar a mão na massa!"
        }
        redirect_back_or user
      else
        flash[:warning] = {
          :type => 'error',
          :title => 'Alto lá!',
          :msg => 'Sua conta ainda não foi ativada. Verifique o e-mail de confirmação e clique no link.'
        }
        redirect_to root_url
      end
    else
      flash[:warning] = {
        :type => 'error',
        :title => 'Usuário/senha não reconhecidos',
        :msg => 'Revise os dados informados e tente novamente.'
      }
      redirect_to login_path
    end
  end

  def destroy
    log_out if logged_in?
    flash[:bye] = {
      :type => 'success',
      :msg => 'Volte sempre, estaremos te esperando.'
    }
    redirect_to login_url
  end
end
