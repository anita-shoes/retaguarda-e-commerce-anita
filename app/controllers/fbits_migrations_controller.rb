class FbitsMigrationsController < ApplicationController

	include PicturesSwapHelper
	include NerusHelper
	include FbitsHelper

	def index
		@page = params[:page].nil? ? 0 : params[:page]
		@size = params[:size].nil? ? 50 : params[:size]
	end

	def products_nerus_2_tray
		@products = []
		nerus_grids(pre_creation_list).each do |code, colors|
			colors.each do |color, skus|
				skus.each do |sku, grid|
					if !sku.nil?
						cleanup_categories(sku)
		    		cleanup_images(sku)

		    		product_fbits = ProductFbits.new_from_nerus(grid)
		    		(has_product(sku)) ? edit_product(product_fbits) : create_product(product_fbits)
						
		    		categories = grid_categories(grid)
						create_product_categories(sku, categories)
					end
	    	end
	    	if skus.count > 0
		    	pictures = list_pictures(skus.values.first[:product_number], skus.values.first[:color_code])
		    	create_product_images(skus.values.first[:sku], pictures) if skus.count > 0

		    	product_fbits = ProductFbits.new_from_nerus(skus.values.first)
		    	@products << Product.create_or_update_from_fbits(product_fbits)
				end
			end
			to_review(code)
		end
	end

	def migrate_clients
		fbitisApiService = FbitsApiService.new
		fbits_clients = fbitisApiService.get_clients(params[:page], params[:size])
		clients = Client.populate_from_fbits(fbits_clients)

		clients.each do |client|
			if client.is_a? Object 
				if !client.nil? and client.id != 1073753
					fbits_adresses = fbitisApiService.get_client_adresses(client.id)
					client.adresses = Address.populate_from_fbits(fbits_adresses)
				
					begin
					  client.save
					rescue ActiveRecord::RecordNotSaved => e
					  client.errors.full_messages
					end
				end
			end
		end
		
		respond_to do |format|
      format.js { "" }
		  render json: { page: params["page"].to_i+1, size: params['size'], go: fbits_clients.any?}
    end
  end
				
  def migrate_brands
		fbitisApiService = FbitsApiService.new
		fbits_brands = fbitisApiService.get_brands()

		fbits_brands.each do |fbits_brand|
			brand = Brand.new

			brand.id = fbits_brand['fabricanteId']
			brand.name = fbits_brand['nome']
			brand.status = 1
			
			brand.save!
		end

		respond_to do |format|
      format.js { "" }
		  render json: { page: nil, size: nil, go: nil}
    end
  end

  def migrate_categories
		fbitisApiService = FbitsApiService.new
		fbits_categories = fbitisApiService.get_categories()

		fbits_categories.each do |fbits_category|
			category = Category.new
			
			category.id = fbits_category['id']
			category.name = fbits_category['nome']
			category.parent_id = (fbits_category['categoriaPaiId'] > 0) ? fbits_category['categoriaPaiId'] : nil
			category.erp_id = fbits_category['categoriaERPId']
			category.status = (fbits_category['ativo'] === 'true') ? 1 : 0
			category.url = fbits_category['urlHotSite']
			category.main_category = fbits_category['categoriaPrincipal']
			
			category.save!
		end

		respond_to do |format|
      format.js { "" }
		  render json: { page: nil, size: nil, go: nil}
    end
  end

  def migrate_products
		fbitisApiService = FbitsApiService.new

		fbits_products = fbitisApiService.get_products(params[:page], params[:size])
		fbits_products.each do |fbits_product|
			product = Product.find_or_create_by(external_parent_id: fbits_product["idPaiExterno"])
		
			product.external_parent_id = fbits_product["idPaiExterno"]
			product.external_link_id = fbits_product["idVinculoExterno"]
			product.name = fbits_product["nome"]
			product.sku = fbits_product["sku"]
			product.show_attributes_matrix = fbits_product["exibirMatrizAtributos"]
			product.author = fbits_product["autor"]
			product.editor = fbits_product["editora"]
			product.collection = fbits_product["colecao"]
			product.gender = fbits_product["genero"]
			product.cost_price = fbits_product["precoCusto"]
			product.from_price = fbits_product["precoDe"]
			product.to_price = fbits_product["precoPor"]
			product.delivery_deadline = fbits_product["prazoEntrega"]
			product.is_valid = fbits_product["valido"]
			product.show_site = fbits_product["exibirSite"]
			product.free_shipping = fbits_product["freteGratis"]
			product.free_exchange = fbits_product["trocaGratis"]
			product.weight = fbits_product["peso"]
			product.height = fbits_product["altura"]
			product.length = fbits_product["comprimento"]
			product.width = fbits_product["largura"]
			product.warranty = fbits_product["garantia"]
			product.max_quantity = fbits_product["quantidadeMaximaCompraUnidade"]
			product.min_quantity = fbits_product["quantidadeMinimaCompraUnidade"]
			product.condition = fbits_product["condicao"]
			product.video_url = fbits_product["urlVideo"]

			fbits_categories = fbitisApiService.get_product_categories(product.sku)
			product.categories.destroy_all
			fbits_categories.each do |fbits_category|
				product.categories << Category.find(fbits_category['id']) if fbits_category.include?("id")
			end
			product.save
		end

		respond_to do |format|
	      format.js { "" }
		  render json: { page: params["page"].to_i+1, size: params['size'], go: fbits_products.any?}
    	end
	end

	def migrate_orders
		fbitisApiService = FbitsApiService.new
		fbits_orders = fbitisApiService.get_orders(params[:page], params[:size])

		if fbits_orders.is_a? Array
			File.write("orders/orders_#{params[:page]}.yml", fbits_orders.to_yaml)

			respond_to do |format|
	      format.js { "" }
			  # render json: { page: params["page"].to_i+1, size: params['size'], go: fbits_orders.any?}
			  render json: { page: params["page"].to_i+1, size: params['size'], go: false}
	    end
	  end
	end

	def migrate_orders_yml
		fbitisApiService = FbitsApiService.new
		fbits_orders = YAML.load_file("orders/orders_#{params[:page]}.yml")

		if fbits_orders.is_a? Array
			orders = Order.populate_from_fbits(fbits_orders)

			orders.each do |order|
				debugger if !order.valid?

				begin
				  order.save
				rescue ActiveRecord::RecordNotSaved => e
				  order.errors.full_messages
				end
			end
		end

		respond_to do |format|
      format.js { "" }
		  render json: { page: params["page"].to_i+1, size: params['size'], go: fbits_orders.any?}
    end
	end

end
