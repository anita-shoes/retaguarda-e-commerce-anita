class Address < ApplicationRecord
	belongs_to :client

	private

		def self.populate_from_fbits(data)
			data.map do |address_json|
				address = Address.find_or_create_by(id: address_json['enderecoId'])

				address_json['id'] = address_json['enderecoId']
				address_json['name'] = address_json['nomeEndereco']
				address_json['street'] = address_json['rua']
				address_json['number'] = address_json['numero']
				address_json['complement'] = address_json['complemento']
				address_json['neighborhood'] = address_json['bairro'].unicode_normalize
				address_json['reference'] = address_json['referencia']
				address_json['city'] = address_json['cidade']
				address_json['state'] = address_json['estado']
				address_json['zip_code'] = address_json['cep']
				address_json['country'] = address_json['pais']

				address_json = address_json.except(
	        "enderecoId",
	        "nomeEndereco",
	        "rua",
	        "numero",
	        "complemento",
	        "refefencia",
	        "bairro",
	        "referencia",
	        "cidade",
	        "estado",
	        "cep",
	        "pais",
	        "utilizadoUltimoPedido"
	      )
	      
	      address.assign_attributes(address_json)
	      address
			end
		end
		
end
