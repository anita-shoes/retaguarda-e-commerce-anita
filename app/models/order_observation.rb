class OrderObservation < ApplicationRecord
	
	private

		def self.populate_from_fbits(data)
			data.map do |json|
				order_observation = OrderObservation.new

				order_observation.text = json['observacao']
      	order_observation.internal = false

      	order_observation

			end
		end

end
