class OrderPayment < ApplicationRecord
	belongs_to :payment_type
		
	has_many :order_payment_infos
	has_many :order_payment_credit_cards
	has_many :order_payment_statuses

	private 
		def self.populate_from_fbits(data)
			data.map do |json|
				order_payment = OrderPayment.new

				order_payment.installment = json['numeroParcelas']
	      order_payment.deduction  = json['valorDesconto']
	      order_payment.installment_value = json['valorParcela']
	      order_payment.interest = json['valorJuros']
	      order_payment.total = json['valorTotal']
	      
	      order_payment.payment_type_id = json['formaPagamentoId']

	      order_payment.order_payment_infos << OrderPaymentInfo.populate_from_fbits(json['informacoesAdicionais'])
	      order_payment.order_payment_credit_cards << OrderPaymentCreditCard.populate_from_fbits(json['cartaoCredito'])
	      order_payment.order_payment_statuses << OrderPaymentStatus.populate_from_fbits(json['pagamentoStatus'])

				order_payment
			end

		end
end
