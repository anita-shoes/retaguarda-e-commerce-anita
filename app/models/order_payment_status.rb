class OrderPaymentStatus < ApplicationRecord

	private 

		def self.populate_from_fbits(data)
			data.map do |json|
				order_payment_status = OrderPaymentStatus.new

				order_payment_status.authorization = json['numeroAutorizacao']
	      order_payment_status.receipt = json['numeroComprovanteVenda']
      	order_payment_status.acquirer = json['adquirente']

				order_payment_status
				
			end
		end
end
