class OrderShipping < ApplicationRecord
	belongs_to :shipping_type
	belongs_to :stock_area

	enum address_type: [:"Entrega", :"Devolução"]

	private 
		def self.populate_from_fbits(address_data, shipping_data)
			shipping_types = {
				53 => 3, 
				99 => 3, 
				100 => 2, 
				4 => 2, 
				3 => 1, 
				41107 => 1, 
				40290 => 2, 
				41106 => 1, 
				4510 => 1, 
				4669 => 1,
				4162 => 1
			}

			address_data.map do |json|
				order_shipping = OrderShipping.new

				order_shipping.address_type = json['tipo']
	      order_shipping.to = json['nome']
	      order_shipping.address = json['endereco']
	      order_shipping.number = json['numero']
	      order_shipping.complement = json['complemento']
	      order_shipping.reference = json['referencia']
	      order_shipping.zip_code = json['cep']
	      order_shipping.neighborhood = json['bairro']
	      order_shipping.city = json['cidade']
	      order_shipping.state = json['estado']
	      order_shipping.country = json['pais']
	      order_shipping.estimate_value = shipping_data['valorFreteEmpresa']
	      order_shipping.charged_value = shipping_data['valorFreteCliente']
	      order_shipping.paid_value = shipping_data['valorFreteCliente']
	      order_shipping.estimated_delivery_time = shipping_data['prazoEnvio']
	      order_shipping.given_delivery_time = shipping_data['prazoEnvio']
	      order_shipping.executed_delivery_time = shipping_data['prazoEnvio']

				shipping_data['freteContratoId'] = 4162 if shipping_types[shipping_data['freteContratoId']].nil?
	      shipping_type = ShippingType.find(shipping_types[shipping_data['freteContratoId']])

				order_shipping.shipping_type = shipping_type
				order_shipping.stock_area_id = 1

	      order_shipping
	    end

		end
end
