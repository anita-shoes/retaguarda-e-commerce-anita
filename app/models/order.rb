class Order < ApplicationRecord
	has_many :shippings, foreign_key: "order_id", class_name: "OrderShipping"
	has_many :payments, foreign_key: "order_id", class_name: "OrderPayment"
	has_many :observations, foreign_key: "order_id", class_name: "OrderObservation"
	has_many :items, foreign_key: "order_id", class_name: "OrderItem"

	belongs_to :order_situation
	belongs_to :client
	belongs_to :store

	private

		def self.populate_from_fbits(data)
			data.map do |json|
				order = Order.find_or_create_by(id: json['pedidoId'])

				order.id = json['pedidoId']
				order.value = json['valorTotalPedido']
				order.deduction = json['valorDesconto']
				order.internal_account = json['valorDebitoCC']
				order.coupon = json['cupomDesconto']
				order.email = json['usuario']['email']
				order.home_phone = json['usuario']['telefoneResidencial']
				order.cell_phone = json['usuario']['telefoneCelular']

				order.client_id = 807609 #json['usuario']['usuarioId']
				order.order_situation_id = json['situacaoPedidoId']
				order.store_id = '2'

				order.shippings.destroy_all
				order.shippings = OrderShipping.populate_from_fbits(json['pedidoEndereco'], json['frete']) if json['pedidoEndereco'].is_a? Array

				order.payments.destroy_all
				order.payments = OrderPayment.populate_from_fbits(json['pagamento'])  if json['pagamento'].is_a? Array

				order.items.destroy_all
				order.items = OrderItem.populate_from_fbits(json['itens']) if json['itens'].is_a? Array

				order.observations.destroy_all
				order.observations = OrderObservation.populate_from_fbits(json['observacao'])

				order.created_at = json['data']
				order.updated_at = json['dataUltimaAtualizacao']

				order

			end

		end
end
