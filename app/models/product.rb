class Product < ApplicationRecord
  enum show_attributes_matrix: [:"Neutro", :"Sim", :"Não"]
  enum condition: [:"Novo", :"Usado", :"Renovado", :"Danificado"]

  # belongs_to :brand
  has_many :attrs, class_name: 'ProductAttribute' 
  has_many :stock
  has_many :infos, class_name: 'ProductInformation'
  has_and_belongs_to_many :categories

  def populate_from_fbits(product_fbits)
    self.external_parent_id = product_fbits.idPaiExterno
    self.external_link_id = product_fbits.idVinculoExterno
    self.name = product_fbits.nome
    self.sku = product_fbits.sku
    self.show_attributes_matrix = product_fbits.exibirMatrizAtributos
    self.author = product_fbits.autor
    self.editor = product_fbits.editora
    self.collection = product_fbits.colecao
    self.gender = product_fbits.genero
    self.cost_price = product_fbits.precoCusto
    self.from_price = product_fbits.precoDe
    self.to_price = product_fbits.precoPor
    self.delivery_deadline = product_fbits.prazoEntrega
    self.is_valid = product_fbits.valido
    self.show_site = product_fbits.exibirSite
    self.free_shipping = product_fbits.freteGratis
    self.free_exchange = product_fbits.trocaGratis
    self.weight = product_fbits.peso
    self.height = product_fbits.altura
    self.length = product_fbits.comprimento
    self.width = product_fbits.largura
    self.warranty = product_fbits.garantia
    self.max_quantity = product_fbits.quantidadeMaximaCompraUnidade
    self.min_quantity = product_fbits.quantidadeMinimaCompraUnidade
    self.condition = product_fbits.condicao
    self.video_url = product_fbits.urlVideo
  end

  def get_color
    external_parent_id[6..-1] if external_parent_id
  end

  private 

  def self.create_or_update_from_fbits(product_fbits)
    product = Product.find_or_create_by(external_parent_id: product_fbits.idPaiExterno)
    product.populate_from_fbits(product_fbits)
    product.save
    return product
  end

end
