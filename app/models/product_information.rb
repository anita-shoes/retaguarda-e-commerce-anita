class ProductInformation < ApplicationRecord

	private 

    def self.populate_from_fbits(informations_data)
    	informations_data.map do |information_json|
    		information = ProductInformation.new
    		information.title = information_json['titulo']
    		information.text = information_json['texto']
    		information.information_type = information_json['tipoInformacao']

    		information
    	end
    end
end
