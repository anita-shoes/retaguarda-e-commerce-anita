class OrderItem < ApplicationRecord
	has_many :order_item_logs
	has_many :order_item_attributes

	belongs_to :product

	private 
		def self.populate_from_fbits(data)
			data.map do |json|
				order_item = OrderItem.new

				order_item.price = json['precoPor']
				order_item.charged = json['precoVenda']
				order_item.gift = json['isBrinde']
				order_item.quantity = json['quantidade']

				order_item.product_id = json['produtoVarianteId']

				order_item.order_item_attributes.destroy_all
				order_item.order_item_attributes = OrderItemAttribute.populate_from_fbits(json['atributos'])

				order_item				
			end
		end
end
