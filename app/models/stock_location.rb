class StockLocation < ApplicationRecord
	enum visible_site: [:"Não", :"Sim"]
	enum status: [:"Inativo", :"Ativo"]
end
