class ProductAttribute < ApplicationRecord
	enum attr_type: [:"Seleção", :"Filtro", :"Comparação", :"Configuração", :"EXCLUSIVO PARA O GOOGLE"]

	private 

    def self.populate_from_fbits(attributes_data)
    	types = {:Selecao => "Seleção", :Filtro => "Filtro", :Comparacao => "Comparação", :Configuracao => "Configuração", :"ExclusivoGoogle" => "EXCLUSIVO PARA O GOOGLE"}
    	
    	attributes_data.map do |attribute_json|
    		attribute = ProductAttribute.new
    		
    		attribute.attr_type = types[attribute_json['tipoAtributo'].to_sym]
    		attribute.is_filter = attribute_json['isFiltro']
    		attribute.name = attribute_json['nome']
    		attribute.value = attribute_json['valor']
    		attribute.show = attribute_json['exibir']

    		attribute
    	end
    end
end
