class BrandFbits

  attr_accessor :nome,
    :urlLogoTipo,
    :urlLink,
    :urlCarrossel

  def initialize(name)
    self.nome = name
    self.urlLogoTipo = ''
    self.urlLink = ''
    self.urlCarrossel = ''
  end
end
