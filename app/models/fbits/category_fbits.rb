class CategoryFbits

  attr_accessor :id,
    :nome,
    :categoriaPaiId,
    :categoriaERPId,
    :ativo,
    :isReseller,
    :exibirMatrizAtributos,
    :exibeMenu,
    :urlHotSite

  def initialize(category, parent, url)
    self.nome = category[:name].titleize
    self.categoriaPaiId = parent
    self.categoriaERPId = category[:id].to_s
    self.ativo = true
    self.isReseller = false
    self.exibirMatrizAtributos = "Neutro"
    self.exibeMenu = true
    self.urlHotSite = url
  end
end
