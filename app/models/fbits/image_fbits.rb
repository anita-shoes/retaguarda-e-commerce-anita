class ImageFbits

  attr_accessor :base64,
    :exibirMiniatura,
    :estampa,
    :formato,
    :ordem

  def initialize(base64, order)
    self.base64 = base64
    self.exibirMiniatura = true
    self.estampa = false
    self.formato = 'jpg'
    self.ordem = order
  end
end
