class ProductFbits

  attr_accessor :idPaiExterno,  
    :idVinculoExterno,
    :sku,
    :nome,
    :nomeProdutoPai,
    :exibirMatrizAtributos,
    :contraProposta,
    :fabricante,
    :autor,
    :editora,
    :colecao,
    :genero,
    :precoCusto,
    :precoDe,
    :precoPor,
    :fatorMultiplicadorPreco,
    :prazoEntrega,
    :valido,
    :exibirSite,
    :freteGratis,
    :trocaGratis,
    :peso,
    :altura,
    :comprimento,
    :largura,
    :garantia,
    :isTelevendas,
    :ean,
    :localizacaoEstoque,
    :listaAtacado,
    :estoque,
    :listaAtributos,
    :quantidadeMaximaCompraUnidade,
    :quantidadeMinimaCompraUnidade,
    :condicao,
    :urlVideo,
    :spot,
    :paginaProduto,
    :marketplace,
    :somenteParceiros,
    :buyBox

  def initialize
    self.exibirMatrizAtributos = "Neutro"
    self.valido = true
    self.exibirSite = true
    self.freteGratis = "Neutro"
    self.trocaGratis = true
    self.estoque = []
    self.listaAtributos = []
    self.condicao = "Novo"
    self.spot = true
    self.paginaProduto = true
    self.marketplace = true
    self.peso = 500
    self.altura = 15
    self.comprimento = 35
    self.largura = 20
  end

  def set_stock_from_nerus(nerus_stock)
    self.estoque << {
      centroDistribuicaoId: 25,
      estoqueFisico: 0    
    }
    
    stock = []
    fisical = 0
    pairs = nerus_stock.split(',')
    pairs.each_with_index do |pair, key|
      grid = pair.split(':')
      fisical = (key == 0) ? (grid.second.to_i - 1) : grid.second.to_i
      self.estoque << {
        centroDistribuicaoId: ProductFbits.get_fbits_stock_id(grid.first),
        estoqueFisico: fisical
      }
    end
  end

  def add_attribute_from_nerus(attribute, type, show=true)
    self.listaAtributos << {
      nome: type,
      valor: attribute,
      exibir: show
    }
  end

  private 

  def self.new_from_nerus(nerus_data)
    color_attribute = (nerus_data[:color_attribute].nil?) ? nerus_data[:color_code].titleize : nerus_data[:color_attribute].titleize 
    product = self.new
    product.idPaiExterno = nerus_data[:product_number]+nerus_data[:color_code]
    product.idVinculoExterno = nerus_data[:product_number]
    product.sku = nerus_data[:sku]
    product.nome = "#{nerus_data[:name].titleize} #{color_attribute} - #{nerus_data[:product_number]}"
    product.fabricante = nerus_data[:brand_name].titleize
    product.precoDe = nerus_data[:initial_price]
    product.precoPor = nerus_data[:final_price]
    
    product.set_stock_from_nerus(nerus_data[:grid])
    product.add_attribute_from_nerus(nerus_data[:color_code], "Código da cor")
    product.add_attribute_from_nerus(color_attribute, "Cor")
    product.add_attribute_from_nerus(nerus_data[:size], "Tamanho")

    product
  end

  def self.get_fbits_stock_id(nerus_id)
    self.get_fbits_stores[nerus_id.to_i]
  end

  def self.get_fbits_stores
    {
      10 => 32,
      12 => 33,
      13 => 34,
      14 => 35,
      19 => 40,
      21 => 39,
      22 => 36,
      26 => 37,
      27 => 41,
      29 => 38
      # 36 => 25
    }
  end

end
