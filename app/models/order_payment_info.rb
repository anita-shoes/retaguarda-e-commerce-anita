class OrderPaymentInfo < ApplicationRecord
		
	private 

		def self.populate_from_fbits(data)
			data.map do |json|
				order_payment_info = OrderPaymentInfo.new

				order_payment_info.key = json['chave']
				order_payment_info.value = json['valor']

				order_payment_info				
			end
		end
end
