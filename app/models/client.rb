class Client < ApplicationRecord
	attr_accessor :remember_token, :reset_token
	has_many :adresses
  before_save :downcase_email
  validates :name, presence: true
  validates :email, presence: true,
                    # format: { with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i },
                    uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  enum type: [:"Física", :"Jurídica"]
  enum origin: [:"Mais Empresas", :"Anita Shoes", :"Tray Corp", :"Mailing list", :Outro]
  enum genre: [:"Masculino", :"Feminino", :Indefinido]

  def usuarioId=(value)
    self.id = value
  end

  private

    def self.populate_from_fbits(data)
      types = {:Fisica => "Física", :Juridica => "Jurídica"}
      genders = {:Masculino => "Masculino", :Feminino => "Feminino", :Undefined => "Indefinido"}

      data.map do |client_json|
        if client_json['email'].length > 6 && client_json['usuarioId'] != 895870
          client = Client.find_or_create_by(email: client_json['email'])
          client_json['id'] = client_json['usuarioId']
          client_json['client_type'] = types[client_json['tipoPessoa'].to_sym]
          client_json['origin'] = client_json['origemContato']
          client_json['genre'] = genders[client_json['tipoSexo'].to_sym]
          client_json['name'] = (client_json['nome'] != "") ? client_json['nome'] : client_json['email']
          client_json['home_phone'] = client_json['telefoneResidencial']
          client_json['cell_phone'] = client_json['telefoneCelular']
          client_json['business_phone'] = client_json['telefoneComercial']
          client_json['birthday'] = client_json['dataNascimento']
          client_json['business_name'] = client_json['razaoSocial']
          client_json['ie'] = client_json['inscricaoEstadual']
          client_json['responsible'] = client_json['responsavel']
          client_json['password'] = client_json['email'].rjust(6, ' ')
          client_json['created_at'] = client_json['dataCriacao']
          client_json['updated_at'] = client_json['dataAtualizacao']

          client_json = client_json.except(
            "usuarioId", 
            "grupoInformacaoCadastral", 
            "tipoPessoa", 
            "origemContato", 
            "tipoSexo", 
            "nome", 
            "telefoneResidencial", 
            "telefoneCelular",
            "telefoneComercial",
            "dataNascimento",
            "razaoSocial",
            "inscricaoEstadual",
            "responsavel",
            "dataCriacao",
            "dataAtualizacao",
            "revendedor",
            "listaInformacaoCadastral"
          )
          
          client.assign_attributes(client_json)
          client
        end
      end
    end

    def downcase_email
      self.email = email.downcase
    end
end
