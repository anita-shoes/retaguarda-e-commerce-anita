class OrderPaymentCreditCard < ApplicationRecord

	private 

		def self.populate_from_fbits(data)
			data.map do |json|
				order_payment_credit_card = OrderPaymentCreditCard.new

				order_payment_credit_card. number = json['numeroCartao']
	      order_payment_credit_card.name = json['nomeTitular']
	      order_payment_credit_card.expiration = json['dataValidade']
	      order_payment_credit_card.security_code = json['codigoSeguranca']
	      order_payment_credit_card.document = json['documentoCartaoCredito']
	      order_payment_credit_card.token = json['token']
	      order_payment_credit_card.info = json['info']
	      order_payment_credit_card.flag = json['bandeira']

	      order_payment_credit_card
				
			end
		end
end
