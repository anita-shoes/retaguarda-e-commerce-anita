class OrderItemAttribute < ApplicationRecord

	private 
		def self.populate_from_fbits(data)
			data.map do |json|
				order_item_attribute = OrderItemAttribute.new

				order_item_attribute.name = json['produtoVarianteAtributoNome']
				order_item_attribute.value = json['produtoVarianteAtributoValor']

				order_item_attribute				
			end
		end
end
