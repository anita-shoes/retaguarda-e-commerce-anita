include ApplicationHelper
include NerusHelper
include FbitsHelper

start_page = 0
page_size = 50
total = Product.count
dbg('Starting PRICE sync, total products', total, :green, :nil, :blink)

pages = (total.to_f/page_size.to_f).ceil
dbg('Pages', pages, :blue, :bold)

for i in start_page...pages do
  dbg('Go page', i, :nil, :nil, :nil)
  product_collection = {}
  products = Product.order(id: :desc).limit(page_size).offset(i*page_size)

  products.each do |product|
    code = product.external_link_id
    color = product.get_color

    product_collection[code] = {} if product_collection[code].nil?
    product_collection[code][color] = {} if product_collection[code][color].nil?
  end

  @price_collection = []
  nerus_grids(product_collection).each do |code, colors|
    colors.each do |color, skus|
      skus.each do |sku, grid|
        @price_collection << {
          identificador: sku,
          precoDe: grid[:initial_price],
          precoPor: grid[:final_price]
        }
        
        if (@price_collection.count == 50)
          update_price(@price_collection)
          dbg("50 Skus Reached", '⏳', :nil, :nil, :blink)
          @price_collection = []
        end
      end
    end
  end
  
  update_price(@price_collection)
  dbg("Saldo final de Skus", '⏳', :nil, :nil, :blink)
end

dbg("FIM, The End, Acabou!!! ", '💪🏼', :green, :nil, :blink)