include ApplicationHelper
include NerusHelper
include FbitsHelper

start_page = 0
page_size = 50

def explode_stock(grid)
  result = []
  stores = []
 reduced = nil
  
  grid.split(',').each do |stock|
    stock = stock.split(":")
    fisical_stock = stock.second.to_i
   if (reduced.nil? && fisical_stock > 0)
     fisical_stock = (stock.second.to_i - 1)
     reduced = true
   end

    result << { 
      centroDistribuicaoId: ProductFbits.get_fbits_stock_id(stock.first),
      estoqueFisico: (fisical_stock >= 0) ? fisical_stock : 0
    }
    stores << stock.first
  end

  ProductFbits.get_fbits_stores.each do |erp, fbits|
    result << { 
      estoqueFisico: 0, 
      centroDistribuicaoId: fbits
    } if !stores.include? erp.to_s
  end

  result
end

total = Product.count
dbg('Starting STOCK sync, total products', total, :green, :nil, :blink)

pages = (total.to_f/page_size.to_f).ceil
dbg('Pages', pages, :blue, :bold)

for i in start_page...pages do
  dbg('Go page', i, :nil, :nil, :nil)
  product_collection = {}
  products = Product.order(id: :desc).limit(page_size).offset(i*page_size)

  products.each do |product|
    code = product.external_link_id
    color = product.get_color

    product_collection[code] = {} if product_collection[code].nil?
    product_collection[code][color] = {} if product_collection[code][color].nil?
  end

  @stock_collection = []
  nerus_grids(product_collection).each do |code, colors|
    colors.each do |color, skus|
      skus.each do |sku, grid|
        @stock_collection << {
          identificador: sku,
          listaEstoque: explode_stock(grid[:grid])
        }
        
        if (@stock_collection.count == 50)
          update_stock(@stock_collection)
          dbg("50 Skus Reached", '⏳', :nil, :nil, :blink)
          @stock_collection = []
        end
      end
    end
  end
  
  update_stock(@stock_collection)
  dbg("Saldo final de Skus", '⏳', :nil, :nil, :blink)
end

dbg("FIM, The End, Acabou!!! ", '💪🏼', :green, :nil, :blink)