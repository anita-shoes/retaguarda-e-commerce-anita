include ApplicationHelper
include NerusHelper
include FbitsHelper

def explode_stock(grid)
  result = []
  stores = []
  reduced = nil
  
  grid.split(',').each do |stock|
    stock = stock.split(":")
    fisical_stock = stock.second.to_i
    if (reduced.nil? && fisical_stock > 0)
      fisical_stock = (stock.second.to_i - 1)
      reduced = true
    end

    result << { 
      centroDistribuicaoId: ProductFbits.get_fbits_stock_id(stock.first),
      estoqueFisico: (fisical_stock >= 0) ? fisical_stock : 0
    }
    stores << stock.first
  end

  ProductFbits.get_fbits_stores.each do |erp, fbits|
    result << { 
      estoqueFisico: 0, 
      centroDistribuicaoId: fbits
    } if !stores.include? erp.to_s
  end

  result
end

product_collection = {}
product = Product.find_by(external_link_id: 227203)

code = product.external_link_id
color = product.get_color

product_collection[code] = {} if product_collection[code].nil?
product_collection[code][color] = {} if product_collection[code][color].nil?

@stock_collection = []
nerus_grids(product_collection).each do |code, colors|
  colors.each do |color, skus|
    skus.each do |sku, grid|
      @stock_collection << {
        identificador: sku,
        listaEstoque: explode_stock(grid[:grid])
      }
      
      if (@stock_collection.count == 50)
        update_stock(@stock_collection)
        dbg("50 Skus Reached", @stock_collection, :nil, :nil, :bold)
        @stock_collection = []
      end
    end
  end
end

if @stock_collection.count > 0
  update_stock(@stock_collection)
  dbg("Saldo final de Skus", @stock_collection, :nil, :nil, :bold)
end
