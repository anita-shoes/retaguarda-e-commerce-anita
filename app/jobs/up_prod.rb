include ApplicationHelper
include NerusHelper
include FbitsHelper
include PicturesSwapHelper


@products = []
new_prods = nerus_grids(pre_creation_list)
dbg('Starting uploading products', new_prods.size, :green, :nil, :blink)
new_prods.each do |code, colors|
    dbg("Go code #{code} with #{colors.size} colors", '😎', :green, :nil, :blink)
    colors.each do |color, skus|
        dbg("Go color #{color} with #{skus.size} SKUs", '😎', :green, :nil, :blink)
        skus.each do |sku, grid|
            if !sku.nil?
                cleanup_categories(sku)

            cleanup_images(sku)
            product_fbits = ProductFbits.new_from_nerus(grid)
            (has_product(sku)) ? edit_product(product_fbits) : create_product(product_fbits)
                
            categories = grid_categories(grid)
                create_product_categories(sku, categories)
            end
    end
    if skus.count > 0
        pictures = list_pictures(skus.values.first[:product_number], skus.values.first[:color_code])
        create_product_images(skus.values.first[:sku], pictures) if skus.count > 0

        product_fbits = ProductFbits.new_from_nerus(skus.values.first)
        @products << Product.create_or_update_from_fbits(product_fbits)
        end
    end
    to_review(code)
    dbg("DONE!!! Next...", '💪🏼', :green, :nil, :blink)
end

dbg("FIM, The End, Acabou!!! ", '💪🏼', :green, :nil, :blink)
